<?php

require ("./global.php");
require ($site_abspath . "/header.tpl");

echo '<table><tr><td align="right">';

if (isset($_POST['string'])) {
	$string = $_POST['string'];
	$hash_md5 = hash('md5', $string);
	$hash_sha1 = hash('sha1', $string);
	$hash_sha2 = hash('sha256', $string);
  echo '<br /><b>MD5:</b> <input name="hash_md5" type="text" id="hash_md5" size="80" value="' . $hash_md5 . '" disabled><br />';
  echo '<b>SHA-1:</b> <input name="hash_md5" type="text" id="hash_md5" size="80" value="' . $hash_sha1 . '" disabled><br />';
  echo '<b>SHA-256:</b> <input name="hash_md5" type="text" id="hash_md5" size="80" value="' . $hash_sha2 . '" disabled><br /><br />';
}

?>

<form name="user_info" method="post" action="<?php echo $thispage ?>">
  <input name="string" type="text" id="string" size="40" maxlength="80">
  <input type="submit" name="submit" value="Generate Hash">
</form>

<?php

echo '</td></tr></table>';

require ($site_abspath . "/footer.tpl");

?>
