<?php

session_start();

require_once ('./global.php');
// require header is a bit further down

// check if 'nav' is set (for selecting a tab)
if (isset($_GET['nav'])) $pgNav = $_GET['nav'];
else $pgNav = 'servers';

// this will update the reload button in the navigation bar to point to last used tab
$thisPageNav = $thisPage;
$thisPage .= '?nav=' . $pgNav;

##########################################
## Choose to auto select a specific tab
##
switch ($pgNav) {
	case 'servers':
		$pgNavSelect = 'setupServers';
		break;
	case 'ports':
		$pgNavSelect = 'setupPorts';
		break;
	case 'users':
		$pgNavSelect = 'setupUsers';
		break;
	case 'groups':
		$pgNavSelect = 'setupGroups';
		break;
	default:
		$pgNavSelect = 'setupServers';
}

require_once ($site_abspath . '/header.tpl');

//if ($_POST['submit'] == 'Update Account') {
  //update_user($_POST['username'], $_POST['password'], $_POST['pwconfirm'], $_POST['email'], $_POST['email_notify'], $_POST['firstname'], $_POST['lastname'], $userinfo->admin_lvl, $_POST['icq'], $_POST['icq_notify'], $_POST['phone'], $_POST['sms'], $_POST['sms_notify']);
//}

if ($userinfo->userid != 0) {

  if (DEBUG){
    //print_r($userinfo);
    echo '<br><center>';
    print_r($_POST);
    echo '<br>';
    print_r($_GET);
    echo '</center><br>';
  }

  if ($userinfo->admin_lvl > ADMINLVL_USER) {

    if ($userinfo->admin_lvl >= ADMINLVL_MASTER)
    {
      $adminlvl_selection = '
                      <option value="10" selected>Regular User</option>
                      <option value="300">Group Admin</option>
                      <option value="500">Super Admin</option>
                      ';
    }
    else
    {
      $adminlvl_selection = '
                      <option value="10" selected>Regular User</option>
                      <option value="300">Group Admin</option>
                      ';
    }

    /* following are events that occur when certain buttons are clicked */

    if ($userinfo->admin_lvl >= ADMINLVL_MASTER) {   // only master admins have access to these functions

      if (isset($_POST['submit_group_add'])) {
        if ($_POST['group'] != NULL) {
          $notifyMsg .= notify_msg('Proceeding to add new group `' . $_POST['group'] . '` . . .', 'notify', true);
          group_add($_POST['group']);
        }
        else {
          $notifyMsg .= notify_msg('Error: You need to specify a group name.', 'error', true);
        }
      }
      elseif (isset($_POST['submit_group_update'])) {
        group_update($_POST['select_group'], $_POST['group_newname']);
      }
      elseif (isset($_POST['submit_group_delete'])) {
        if ($_POST['select_group'] != NULL) {
          $notifyMsg .= notify_msg('Proceeding to delete group with id `' . $_POST['select_group'] . '`. Please be aware that this system will only delete the group itself, all users and servers will remain in the system but will be listed under unassigned group.', 'notify', true);
          group_delete($_POST['select_group']);
        }
        else {
          $notifyMsg .= notify_msg('Error: You need to specify a group name.', 'error', true);
        }
      }

    }


    if (isset($_POST['submit_server_add'])) {
      if (isset($_POST['hostname']) && isset($_POST['select_group'])) {
        $notifyMsg .= notify_msg('Proceeding to add new server `' . $_POST['hostname'] . '` . . .', 'notify', true);
        server_add($_POST['hostname'], $_POST['select_group']);
      }
      else {
        $notifyMsg .= notify_msg('You need to specify a server and a group, make sure you have both.', 'error', true);
      }
    }
    elseif (isset($_POST['submit_server_update'])) {
      if (isset($_POST['select_server']) && isset($_POST['select_group'])) {
        $notifyMsg .= notify_msg('Proceeding to update server `' . $_POST['select_server'] . '` . . .', 'notify', true);
        server_update($_POST['select_server'], $_POST['select_group']);
      }
      else {
        $notifyMsg .= notify_msg('You need to specify a server and a group, make sure you have both.', 'error', true);
      }
    }
    elseif (isset($_POST['submit_server_delete'])) {
      server_delete($_POST['select_server']);
    }
    elseif (isset($_GET['server_remove_from_group'])) {
      $notifyMsg .= notify_msg('Server being removed from group.', 'notify', true);
      server_remove_from_group($_GET['server_remove_from_group'], $_GET['groupid']);
    }
    elseif (isset($_POST['submit_user_add'])) {
      if (!$_POST['select_group'] && $userinfo->admin_lvl < ADMINLVL_MASTER) {
        $notifyMsg .= notify_msg('You must select a group for this user.', 'error', true);
      }
      else
        $success = user_add($_POST['username'], $_POST['password'], $_POST['confirm'], $_POST['email'], 1, $_POST['select_admin_level']);
        if ($success) {
          $userid = $db->insert_id;
          user_add_to_group($userid, $_POST['select_group']);
        }
    }
    elseif (isset($_POST['submit_user_delete'])) {
      user_delete($_POST['select_user']);
    }
    elseif (isset($_POST['submit_user_add_to_group'])) {
      if ($_POST['select_user'] != "" && $_POST['select_user'] != "")
        user_add_to_group($_POST['select_user'], $_POST['select_group']);
      else
        $notifyMsg .= notify_msg('You must a select both a user and a group.', 'error', true);
    }
    elseif (isset($_GET['user_remove_from_group'])) {
      //$notifyMsg .= notify_msg('Server being removed from group.', 'notify', true);
      user_remove_from_group($_GET['user_remove_from_group'], $_GET['groupid']);
    }

    if ($userinfo->admin_lvl >= ADMINLVL_MASTER) {   // only master admins have access to these functions
			if(!isset($_POST['select_pgroup'])) $_POST['select_pgroup'] = NULL;  // if pgroup isn't set, make it NULL

      if (isset($_POST['submit_pgroup_add'])) {
        pgroup_add($_POST['pgroup']);
      }
      elseif (isset($_POST['submit_pgroup_update'])) {
        pgroup_update($_POST['select_pgroup'], $_POST['pgroup_newname']);
      }
      elseif (isset($_POST['submit_pgroup_delete'])) {
        pgroup_delete($_POST['select_pgroup']);
      }
      elseif (isset($_POST['submit_port_add'])) {
        port_add($_POST['portname'], $_POST['port'], $_POST['select_pgroup']);
      }
      elseif (isset($_POST['submit_port_delete'])) {
        port_delete($_POST['select_port']);
      }
      elseif (isset($_POST['submit_port_add_to_group'])) {
        port_add_to_group($_POST['select_port'], $_POST['select_pgroup']);
      }
      elseif (isset($_GET['port_remove_from_pgroup'])) {
        port_remove_from_group($_GET['port_remove_from_pgroup'], $_GET['pgroupid']);
      }
    }


    /* following are lists generated for select boxs and other lists on this page */

    /* generate general group list, used for select boxes */
    $group_list = '';
    $groups = $db->get_results("SELECT * FROM " . DB_TABLE_GROUPS);
    if ($groups) {
      foreach ( $groups as $group )
      {
        if ($userinfo->admin_lvl < ADMINLVL_MASTER) {
          foreach ($userinfo->usergroups as $usergroup) {
            if ($usergroup->groupid == $group->groupid)
              $group_list .= '<option value="' . $group->groupid . '">' . $group->groupname . '</option>' . "\n";
          }
        }
        else
          $group_list .= '<option value="' . $group->groupid . '">' . $group->groupname . '</option>' . "\n";
      }
    }
		//var_export($groups);

    /* generate general server list, used for select boxes */
    /*
    $server_list = '';
    $servers = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS . " ORDER BY hostname");
    if ($servers) {
      foreach ( $servers as $server )
      {
        $server_list .= '<option value="' . $server->serverid . '">' . $server->hostname . '</option>' . "\n";
      }
    }
  */

    /* generate general port group list, used for select boxes */
    $pgroup_list = '';
    $pgroups = $db->get_results("SELECT * FROM " . DB_TABLE_PGROUPS);
    if ($pgroups) {
      foreach ( $pgroups as $pgroup )
      {
        $pgroup_list .= '<option value="' . $pgroup->pgroupid . '">' . $pgroup->pgroupname . '</option>';
      }
    }

    function specific_server_list($group_id = 'all') {
      /* generate the specific list of servers (specifc as in: with ip address and other info) */
      global $db, $thisPage, $thisScript, $site_url, $global;

      if ($group_id != 'all')
        $set_server_group = " WHERE groupid='$group_id'";
			else
				$set_server_group = '';

      $specific_server_list = '';
      $specific_server_groups = $db->get_results("SELECT * FROM " . DB_TABLE_SERVER_GROUP . $set_server_group . " ORDER BY groupid");
      if ($specific_server_groups) {
				$global->last_server_setup_list_color_flag = 0;
        foreach ( $specific_server_groups as $specific_server_group )
        {
          //if ($specific_server_group->groupid != $global->last_server_setup_list_group_id) {
            if ($global->last_server_setup_list_color_flag == 0) {
              $global->last_server_setup_list_color_flag = 1;
              $global->last_server_setup_list_color = '#99CCFF';
            }
            else {
              $global->last_server_setup_list_color_flag = 0;
              $global->last_server_setup_list_color = '#BBDDFF';
            }
            //$global->last_server_setup_list_group_id = $specific_server_group->groupid;
          //}
          //else {
          //  $global->last_server_setup_list_group_id = $specific_server_group->groupid;
          //}

          $serverRowColor = $global->last_server_setup_list_color;

          if ($specific_server_group->groupid != 0) {
            $group_name = $db->get_var("SELECT groupname FROM " . DB_TABLE_GROUPS . " WHERE groupid='$specific_server_group->groupid'");
          }
          else {
            $group_name = "UNASSIGNED";
          }
          $specific_server = $db->get_row("SELECT * FROM " . DB_TABLE_SERVERS . " WHERE serverid='$specific_server_group->serverid'");
          if ($specific_server->servername)
            $sv_lst_dsply_name = $specific_server->servername;
          else
            $sv_lst_dsply_name = $specific_server->hostname;
          $specific_server_list .= '<tr align="center"><td bgcolor="' . $serverRowColor . '">' . $sv_lst_dsply_name . '</td><td bgcolor="' . $serverRowColor . '">' . $specific_server->ip_address . '</td><td bgcolor="' . $serverRowColor . '">' . $group_name . '</td><td align="right" bgcolor="'. $serverRowColor .'" class="tip text-small">[<a href="'. $thisScript ."?nav=servers&server_remove_from_group=$specific_server->serverid&groupid=$specific_server_group->groupid" . '" onclick="return confirm(\'This will remove server '. $specific_server->hostname .' ('. $specific_server->ip_address .') from group '. $group_name .', are you sure?\');" title="Removes this server from this group, it does not delete the server.">Remove</a>]' . ' [<a href="' . "$site_url/server-edit.php?serverid=$specific_server->serverid" . '">Edit</a>]&nbsp;</td></tr>' . "\n";
        }
      }
      else {
        $specific_server_list .= '<tr align="center"><td colspan="4" bgcolor="#BBDDFF">----- No Results -----</td></tr>' . "\n";
      }

      return $specific_server_list;
    }

    //$specific_server_list = specific_server_list();

    $server_list = '';
    //$serverinfo = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS . ' ORDER BY hostname');
    if ($userinfo->admin_lvl <= ADMINLVL_GROUP) {
      if ($_POST['submit_server_group_from'] && $_POST['select_server_group_from'] != 'all') {
        $specific_server_list = specific_server_list($_POST[select_server_group_from]);
      }
      else {
        foreach ($userinfo->usergroups as $usergroup) {
          if ($usergroup->groupid != 0)
          {
            $specific_server_list = specific_server_list($usergroup->groupid);
          }
          else
            $specific_server_list = '<tr align="center"><td bgcolor="#BBDDFF">' . $serverinfo->hostname . '</td><td bgcolor="#BBDDFF">' . $serverinfo->email . '</td><td bgcolor="#BBDDFF">UNASSIGNED</td><td bgcolor="#BBDDFF">' . $serverinfo->admin_lvl . '</td><td bgcolor="#BBDDFF" class="text-small">[<a href="' . "$site_url/server-edit.php?serverid=$serverinfo->serverid" . '">Edit</a>]</td></tr>' . "\n";
        }
      }
      $servers = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS . ' ORDER BY hostname');
      foreach ( $servers as $server ) {
        $group_match = 0;
        $server_groups = $db->get_results("SELECT groupid FROM " . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server->serverid'");
        if ($server->servername)
          $sv_optn_dsply_name = "$server->servername ($server->ip_address)";
        else
          $sv_optn_dsply_name = $server->hostname;
        //print_r($server_groups);
        //echo '<br>';
        if ($server_groups) {
          foreach ( $userinfo->usergroups as $usergroup ) {
            foreach ( $server_groups as $server_group ) {
              //print_r($server_group);
              //echo '<br>';
              if ($usergroup->groupid == $server_group->groupid && $group_match == 0 && $usergroup->groupid != 0) {
                $server_list .= '<option value="' . $server->serverid . '">' . $sv_optn_dsply_name . '</option>' . "\n";
                $group_match = 1;
              }
              elseif ($usergroup->groupid == $server_group->groupid && $group_match == 0) {
                $server_list = '<option value="0">None</option>' . "\n";
                $group_match = 1;
              }
            }
          }
          $group_match = 0;
        }
      }
    }
    elseif ($userinfo->admin_lvl >= ADMINLVL_MASTER) {
      if (isset($_POST['submit_server_group_from']) && $_POST['select_server_group_from'] != 'all') {
        $specific_server_list = specific_server_list($_POST[select_server_group_from]);
      }
      else {
        $specific_server_list = specific_server_list();
      }
      $servers = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS . ' ORDER BY hostname');
      if ($servers){
        foreach ( $servers as $server ) {
          if ($server->servername)
            $sv_optn_dsply_name = "$server->servername ($server->ip_address)";
          else
            $sv_optn_dsply_name = $server->hostname;
          $server_list .= '<option value="' . $server->serverid . '">' . $sv_optn_dsply_name . '</option>' . "\n";
        }
      }
    }



    function specific_user_list($group_id = 'all') {
      /* generate the specific list of users (specifc as in: with email address and other info) */
      global $db, $thisPage, $thisScript, $site_url, $global;

      if ($group_id && $group_id != 'all')
        $set_user_group = " WHERE groupid='$group_id'";
      else
				$set_user_group = '';

      $specific_user_list = "<tbody>\n";
      $specific_user_groups = $db->get_results("SELECT * FROM " . DB_TABLE_USER_GROUP . $set_user_group . " ORDER BY groupid");

			$global->last_user_setup_list_color_flag = 0;
      if (isset($specific_user_groups) && !empty($specific_user_groups)) {
        foreach ( $specific_user_groups as $specific_user_group )
        {
          /************** begin color alternating groups */
          //if ($specific_user_group->groupid != $global->last_user_setup_list_group_id) {
            if ($global->last_user_setup_list_color_flag == 0) {
              $global->last_user_setup_list_color_flag = 1;
              $global->last_user_setup_list_color = 'table-row-edit';
            }
            else {
              $global->last_user_setup_list_color_flag = 0;
              $global->last_user_setup_list_color = 'table-row-edit-alt';
            }

            $global->last_user_setup_list_group_id = $specific_user_group->groupid;
          //}
          //else {
          //  $global->last_user_setup_list_group_id = $specific_user_group->groupid;
          //}

          $userRowColor = $global->last_user_setup_list_color;
          /* end color alternating groups **************/

          if ($specific_user_group->groupid != 0) {
            $group_name = $db->get_var("SELECT groupname FROM " . DB_TABLE_GROUPS . " WHERE groupid='$specific_user_group->groupid'");
          }
          else {
            $group_name = "UNASSIGNED";
          }
          $specific_user = $db->get_row("SELECT * FROM " . DB_TABLE_USERS . " WHERE userid='$specific_user_group->userid'");
          $specific_user_list .= '<tr align="center"><td class="' . $userRowColor . '">' . $specific_user->username . '</td><td class="' . $userRowColor . '">' . $specific_user->email . '</td><td class="' . $userRowColor . '">' . $group_name . '</td><td class="' . $userRowColor . '">' . $specific_user->admin_lvl . '</td><td align="right" class="'. $userRowColor .' tip text-small">[<a href="'. $thisScript ."?nav=users&user_remove_from_group=$specific_user->userid&groupid=$specific_user_group->groupid" . '" title="Removes this user from this group, it does not delete the user." ' . "onclick=\"return confirm('This will remove user $specific_user->username from group $group_name, are you sure?');\">Remove</a>] " . '[<a href="' . "$site_url/user-edit.php?userid=$specific_user->userid" . '">Edit</a>]&nbsp;</td></tr>' . "\n";
        }
      }
      else {
        $specific_user_list .= '<tr align="center"><td colspan="5" bgcolor="#BBDDFF">----- No Results -----</td></tr>' . "\n";
      }
      $specific_user_list .= "</tbody>\n";
      return $specific_user_list;
    }

    $user_list = '';
    if ($userinfo->admin_lvl <= ADMINLVL_GROUP) {
      if ($_POST['submit_user_group_from'] && $_POST['select_user_group_from'] != 'all') {
        $specific_user_list = specific_user_list($_POST[select_user_group_from]);
      }
      else {
        foreach ($userinfo->usergroups as $usergroup) {
          if ($usergroup->groupid != 0)
            $specific_user_list = specific_user_list($usergroup->groupid);
          else
            $specific_user_list = '<tr align="center"><td bgcolor="#BBDDFF">' . $userinfo->username . '</td><td bgcolor="#BBDDFF">' . $userinfo->email . '</td><td bgcolor="#BBDDFF">UNASSIGNED</td><td bgcolor="#BBDDFF">' . $userinfo->admin_lvl . '</td><td bgcolor="#BBDDFF" class="text-small">[<a href="' . "$site_url/user-edit.php?userid=$userinfo->userid" . '">Edit</a>]</td></tr>' . "\n";
        }
      }
      $users = $db->get_results("SELECT * FROM " . DB_TABLE_USERS . " WHERE userid<>'1' ORDER BY username");
      foreach ( $users as $user ) {
        $group_match = 0;
        $user_groups = $db->get_results("SELECT groupid FROM " . DB_TABLE_USER_GROUP . " WHERE userid='$user->userid'");
        if ($user_groups) {
          foreach ( $userinfo->usergroups as $usergroup ) {
            foreach ( $user_groups as $user_group ) {
              if ($usergroup->groupid == $user_group->groupid && $group_match == 0 && $usergroup->groupid != 0 && $user->userid != 1) {
                $user_list .= '<option value="' . $user->userid . '">' . $user->username . '</option>' . "\n";
                $group_match = 1;
              }
              elseif ($usergroup->groupid == $user_group->groupid && $group_match == 0 && $user->userid != 1) {
                $user_list = '<option value="' . $userinfo->userid . '">' . $userinfo->username . '</option>' . "\n";
                $group_match = 1;
              }
            }
          }
          $group_match = 0;
        }
      }
    }
    elseif ($userinfo->admin_lvl >= ADMINLVL_MASTER) {
      if (isset($_POST['submit_user_group_from']) && $_POST['select_user_group_from'] != 'all') {
        $specific_user_list = specific_user_list($_POST['select_user_group_from']);
      }
      else {
        $specific_user_list = specific_user_list();
      }
      $users = $db->get_results("SELECT * FROM " . DB_TABLE_USERS . " WHERE userid<>'1' ORDER BY username");
      if ($users) {
        foreach ( $users as $user ) {
          $user_list .= '<option value="' . $user->userid . '">' . $user->username . '</option>' . "\n";
        }
      }
    }

    /* generate the specific list of ports (specifc as in: with pgroup name and other info) */
    //if ($_POST['submit_user_pgroup_from'] && $_POST['select_user_pgroup_from'] != 'all') {
    //  $set_user_pgroup = " WHERE pgroupid='$_POST[select_user_pgroup_from]'";
    //}
    //$user_list = '';
    $sort_ports_by = 'pgroupid';
    /*
    switch ($_GET['sort_ports']) {
      case 'name':
        $sort_ports_by = 'portname';
        break;
      case 'port':
        $sort_ports_by = 'port';
        break;
      case 'group':
        $sort_ports_by = 'pgroupid';
        break;
      default:
        $sort_ports_by = 'pgroupid';
    }
    */
    //if ($userinfo->admin_lvl < ADMINLVL_MASTER) {

		// I assume this is to select displaying ports only from a specific group but this doesn't seem to be assigned
		// anywhere so I have added this "blank" assignment until proper assignment can be addeda at a later time
		$set_port_group = '';  // will just select all ports
    $specific_port_list = '';
    $specific_port_groups = $db->get_results('SELECT * FROM ' . DB_TABLE_PORT_GROUP . $set_port_group . ' ORDER BY ' . $sort_ports_by . ';');

    if (isset($specific_port_groups) && $specific_port_groups != null)
		{
			$portModRowId = 1;
			foreach ( $specific_port_groups as $specific_port_group )
      {
        if ($specific_port_group->pgroupid != 0) {
          $pgroup_name = $db->get_var("SELECT pgroupname FROM " . DB_TABLE_PGROUPS . " WHERE pgroupid='$specific_port_group->pgroupid';");
        }
        else {
          $pgroup_name = "UNASSIGNED";
        }
        //$port = $db->get_row("SELECT * FROM " . DB_TABLE_PORTS . " WHERE portid='$specific_port_group->portid'");
        //$port_list .= '<option value="' . $port->portid . '">' . $port->portname . " ($port->port)" . '</option>' . "\n";


				$specific_port = $db->get_row("SELECT * FROM " . DB_TABLE_PORTS . " WHERE portid='$specific_port_group->portid';");
        $specific_port_list .= '<tr id="portRow'. $portModRowId .'" align="center">
          <td id="portName'. $portModRowId .'" bgcolor="#BBDDFF">' . $specific_port->portname . '</td>
          <td id="portNum'. $portModRowId .'" bgcolor="#BBDDFF">' . $specific_port->port . '</td>
          <td id="portGroup'. $portModRowId .'" bgcolor="#BBDDFF">' . $pgroup_name . '</td>
          <td id="portModify'. $portModRowId .'" bgcolor="#BBDDFF" class="text-small" align="right"><input type="button" id="portModifySave'. $portModRowId .'" value="Save"> [<a href="'. $thisScript ."?nav=ports&port_remove_from_pgroup=$specific_port->portid&pgroupid=$specific_port_group->pgroupid" . "\" onclick=\"return confirm('This will remove port $specific_port->portname ($specific_port->port) from group $pgroup_name, are you sure?');\">" . 'Remove</a>] [<a id="portEdit'. $portModRowId .'">Edit</a>]&nbsp;</td>
</tr>' . "\n";

				$jQueryFunctions .= <<<JQ

	//########## PORT ROW $portModRowId BEGIN ##########
	$('#portEdit$portModRowId').click(function(){
		$('#portName$portModRowId').html('<input type="text" id="portEditName$portModRowId" size="5" maxlength="50" style="text-align:center;" value="$specific_port->portname">');
		$('#portNum$portModRowId').html('<input type="text" id="portEditNum$portModRowId" size="5" maxlength="5" style="text-align:center;" value="$specific_port->port">');
		// $('#portGroup$portModRowId').html('<select id="portEditGroup$portModRowId">$pgroup_list</select>');
		$('#portGroup$portModRowId').html('^ Use [Set to Group] above ^');
		$('#portModifySave$portModRowId').toggle();
	});
	$('#portModifySave$portModRowId').click(function(){
		var pNum = $('#portEditNum$portModRowId').val();
		var pName = $('#portEditName$portModRowId').val();
		$.post("ajax-calls.php", { portID: $specific_port->portid, portName: pName, portNum: pNum, portGroup: '$pgroup_name', modID: $portModRowId },
		function(data, status){
			$('#portRow$portModRowId').html(data);
		});
	});

	$(document).ready(function() {
		$('#portModifySave$portModRowId').hide();
	});
	//########## PORT ROW $portModRowId END ############

JQ;
			$portModRowId++;
			}
    }
    else {
      $specific_port_list .= '<tr align="center"><td colspan="5" bgcolor="#BBDDFF">----- No Results -----</td></tr>' . "\n";
    }

		$jQueryFunctions .= <<<JQ
JQ;

		/* get port list for option select boxes */
		$port_list = '';
    $ports = $db->get_results('SELECT * FROM ' . DB_TABLE_PORTS . ' ORDER BY port');
    if ($ports){
      foreach ( $ports as $port ) {
        $port_list .= '<option value="' . $port->portid . '">' . $port->portname . " ($port->port)" . '</option>' . "\n";
      }
    }

  } //end group admin only code

	// Check to see if any groups exist. At least one group is required.
	if (!$groups) {
		$noGroupsNotice = '<div style="width: 80%; margin-left: auto; margin-right: auto; padding: 5px; background-color: #f0f0f0; border: 3px solid #770000; text-align: center;">
  There are no Groups defined. At least one group is required to start using netMon.<br><br>
  We suggest \'Main\' or \'Default\'.<br><br>
  Once you have created a Group, this notice will vanish. Like magic...
  </div><br>';
		$pgNav = 'groups';
	}
	else $noGroupsNotice = '';

//select a tab based on 'nav'
 	$jQueryFunctions .= <<<JQ
	$(document).ready(function() {
		var goToTab = $('#setupTabs a[href="#$pgNavSelect"]').parent().index();
		$('#setupTabs').tabs({ active: goToTab });
	});
JQ;

$htmlBody = <<<MAIN_HTML
	<div class="section">
			<div id="setupTabs">
			<ul>
				<li><a href="#setupServers">Servers</a></li>
				<li><a href="#setupPorts">Ports</a></li>
				<li><a href="#setupUsers">Users</a></li>
				<li><a href="#setupGroups">Groups</a></li>
			</ul>
MAIN_HTML;
if ($userinfo->admin_lvl >= ADMINLVL_MASTER) {
	$thisPageNav = $thisScript . '?nav=groups';
$htmlBody .= <<<EDIT_GROUPS
      <div id="setupGroups">
			$noGroupsNotice
      <table class="sub-section">
        <tr>
          <td align="center" class="table-heading">
            <b>Group Setup</b>
          </td>
        </tr>
        <tr>
          <form name="form_group_add" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              <input type="text" name="group" size="25" maxlength="50">
              &nbsp;
              <input type="submit" name="submit_group_add" value="Add New Group">
            </td>
          </form>
        </tr>
        <tr>
          <form name="form_group_modify" method="post" action="$thisPageNav">
            <td align="center" valign="bottom">
              <br>
              <select name="select_group">
$group_list
              </select>
              <b>&rarr;</b>
              <input type="text" name="group_newname" size="20" maxlength="50">
              &nbsp;
              <input type="submit" name="submit_group_update" value="Update">
              <input type="submit" name="submit_group_delete" value="Delete Group" onclick="return confirm('Are you sure you wish to delete this group?');">
              &nbsp;<img src="$images_url/question_mark.png" height="16" width="16" border="0" class="tip" title="<b>Delete Groups or Change Group Names</b><hr><b>Change or Update a Group:</b> simply select the group in the first box, enter the new name in the second box and hit Update.<br><br><b>Delete a Group:</b> select the group to delete in the first box and hit Delete Group.<br><br><strong>Notes on Deleting Groups</strong><br>Deleting a group will not delete any servers or users associated with that group, you will need to delete them manually.">
            </td>
          </form>
        </tr>
      </table>
      </div>
EDIT_GROUPS;
}
// SERVERS
if ($userinfo->admin_lvl >= ADMINLVL_GROUP) {
	$thisPageNav = $thisScript . '?nav=servers';
$htmlBody .= <<<EDIT_SERVERS
      <div id="setupServers">
      <table class="sub-section">
        <tr>
          <td align="center" class="table-heading">
            <b>Server Setup</b>
          </td>
        </tr>
        <tr>
          <form name="form_server_add" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              Hostname:
              <input type="text" name="hostname" size="25" maxlength="80">
              <b>&rarr;</b>
              <select name="select_group">
$group_list
              </select>
              &nbsp;
              <input type="submit" name="submit_server_add" value="Add Server">
            </td>
          </form>
        </tr>
        <tr>
          <form name="form_server_modify" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              <select name="select_server">
$server_list
              </select>
              <b>&rarr;</b>
              <select name="select_group">
$group_list
              </select>
              &nbsp;
              <input type="submit" name="submit_server_update" value="Set to Group">
              <input type="submit" name="submit_server_delete" value="Delete Server" onclick="return confirm('Are you sure you wish to delete this server?');")>
            </td>
          </form>
        </tr>
      </table>
      &nbsp;
      <table class="sub-section">
        <tr align="center">
          <form name="form_server_list_order" method="post" action="$thisPageNav">
          <td colspan="4">
            &nbsp; List Servers From Group:
            <select name="select_server_group_from">
              <option value="all">ALL GROUPS</option>
$group_list
            </select>
            &nbsp;
            <input type="submit" name="submit_server_group_from" value="Go">
          </td>
          </form>
        </tr>
        <tr align="center">
          <td bgcolor="#BBDDFF">
            <b>Hostname (Server)</b>
          </td>
          <td bgcolor="#BBDDFF">
            <b>IP Address</b>
          </td>
          <td bgcolor="#BBDDFF">
            <b>Assigned Group</b>
          </td>
          <td align="right" bgcolor="#BBDDFF">
            <b>Control&nbsp;&nbsp;&nbsp;</b>
          </td>
        </tr>
$specific_server_list
      </table>
      </div>
EDIT_SERVERS;
}
// USERS
if ($userinfo->admin_lvl >= ADMINLVL_GROUP) {
	$thisPageNav = $thisScript . '?nav=users';
$htmlBody .= <<<EDIT_USERS
      <div id="setupUsers">
      <table class="sub-section">
        <tr>
          <td align="center" class="table-heading">
            <b>User Setup</b>
          </td>
        </tr>
        <tr>
          <form name="form_user_add" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              <table width="460" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    Username:
                  </td>
                  <td>
                    <input type="text" name="username" size="20" maxlength="50" tabindex="1">
                  </td>
                  <td>
                    Password:
                  </td>
                  <td>
                    <input type="password" name="password" size="20" maxlength="64" tabindex="3">
                  </td>
                </tr>
                <tr>
                  <td>
                    Email:
                  </td>
                  <td>
                    <input type="text" name="email" size="20" maxlength="50" tabindex="2">
                  </td>
                  <td>
                    Confirm:
                  </td>
                  <td>
                    <input type="password" name="confirm" size="20" maxlength="64" tabindex="4">
                  </td>
                </tr>
                <tr>
                  <td>
                    Admin Type:
                  </td>
                  <td>
                    <select name="select_admin_level" tabindex="5">
$adminlvl_selection
                    </select>
                  </td>
                  <td>
                    Assign Group:
                  </td>
                  <td>
                    <select name="select_group" tabindex="6">
$group_list
                    </select>
                  </td>
                </tr>
              </table>
              <input type="submit" name="submit_user_add" value="Add User" tabindex="7">
              &nbsp;<img src="$images_url/question_mark.png" height="16" width="16" border="0" class="tip" title="<b>User Administration</b><hr>Creating a user and assigning them to a group will allow them to be alerted to all server status changes in that group.<br><br> Users can login to manage their own alert info and preferences as well as view history for servers they are assigned to.<br><br><b>Group Admins</b><br>Group Admins simply have the authority to add additional users to that group.<br><br><b>Super Admins</b><br>Super Admins are the only ones able to add new servers to be monitored.">
            </td>
          </form>
        </tr>
        <tr>
          <form name="form_user_modify" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              <select name="select_user">
$user_list
              </select>
              <b>&rarr;</b>
              <select name="select_group">
$group_list
              </select>
              &nbsp;
              <input type="submit" name="submit_user_add_to_group" value="Set to Group">
              <input type="submit" name="submit_user_delete" value="Delete User" onclick="return confirm('Are you sure you wish to delete this user?');")>
            </td>
          </form>
        </tr>
      </table>
      &nbsp;
      <table class="sub-section">
				<thead>
        <tr align="center">
          <form name="user_setup" method="post" action="$thisPageNav">
            <td colspan="5">
              List Users From Group:
              <select name="select_user_group_from">
                <option value="all">ALL GROUPS</option>
$group_list
              </select>
              &nbsp;
              <input type="submit" name="submit_user_group_from" value="Go">
            </td>
          </form>
        </tr>
        <tr align="center">
          <td bgcolor="#BBDDFF">
            <b>Username</b>
          </td>
          <td bgcolor="#BBDDFF">
            <b>Email</b>
          </td>
          <td bgcolor="#BBDDFF">
            <b>Assigned Group</b>
          </td>
          <td bgcolor="#BBDDFF">
            <b>Admin Type</b>
          </td>
          <td align="right" bgcolor="#BBDDFF">
            <b>Control&nbsp;&nbsp;&nbsp;</b>
          </td>
        </tr>
				</thead>
$specific_user_list
      </table>
			</div>
EDIT_USERS;
}
if ($userinfo->admin_lvl >= ADMINLVL_MASTER) {
	$thisPageNav = $thisScript . '?nav=ports';
$htmlBody .= <<<EDIT_PORT_GROUPS
      <div id="setupPorts">
      <table class="sub-section">
        <tr>
          <td align="center" class="table-heading">
            <b>Port Group Setup</b>
          </td>
        </tr>
        <tr>
          <form name="form_pgroup_add" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              <input type="text" name="pgroup" size="25" maxlength="50">
              &nbsp;
              <input type="submit" name="submit_pgroup_add" value="Add New Port Group">
							&nbsp;<img src="$images_url/question_mark.png" height="16" width="16" border="0" class="tip" title="<b>Port Groups</b><hr>Start by creating a new port group then in the box below create new ports and assign them to groups.<br><br>Once you have a port group created you can then go to the Servers tab, click [Edit] on an existing server and assign either a port group or individual port to be monitored.">
            </td>
          </form>
        </tr>
        <tr>
          <form name="form_pgroup_modify" method="post" action="$thisPageNav">
            <td align="center">
              <br>
              <select name="select_pgroup">
$pgroup_list
              </select>
              <b>&rarr;</b>
              <input type="text" name="pgroup_newname" size="20" maxlength="50">
              &nbsp;
              <input type="submit" name="submit_pgroup_update" value="Update">
              <input type="submit" name="submit_pgroup_delete" value="Delete Group" onclick="return confirm('Are you sure you wish to delete this group?');">
            </td>
          </form>
        </tr>
      </table>
      &nbsp;<hr>&nbsp;
EDIT_PORT_GROUPS;

########## Split Between Port Groups & Ports List ##########

$htmlBody .= <<<EDIT_PORTS
      <table class="sub-section">
        <tr>
          <td align="center" class="table-heading">
            <b>Port Setup</b>
          </td>
        </tr>
        <tr>
          <form name="form_port_add" method="post" action="$thisPageNav">
            <td align="center">
            <table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td>
                  Port Name:
                </td>
                <td>
                  Port:
                </td>
              </tr>
              <tr>
                <td valign="bottom">
                  <input type="text" name="portname" size="12" maxlength="50">&nbsp;&nbsp;
                </td>
                <td valign="bottom">
                  <input type="text" name="port" size="6" maxlength="5">
                  <b>&rarr;</b>
                  <select name="select_pgroup">
$pgroup_list
                  </select>
                  &nbsp;
                  <input type="submit" name="submit_port_add" value="Add New Port">
                </td>
              </tr>
            </table>
            </td>
          </form>
        </tr>
        <tr>
          <form name="form_port_modify" method="post" action="$thisPageNav">
            <td align="center">
              <br \>
              <select name="select_port">
$port_list
              </select>
              <b>&rarr;</b>
              <select name="select_pgroup">
$pgroup_list
              </select>
              &nbsp;
              <input type="submit" name="submit_port_add_to_group" value="Set to Group">
              <input type="submit" name="submit_port_delete" value="Delete Port">
            </td>
          </form>
        </tr>
      </table>
      &nbsp;
      <table class="sub-section">
        <!--<tr align="center">
          <td colspan="3" bgcolor="#CCCCCC" class="text-small">
            Sort By These Columns By Clicking Them
          </td>
          <td class="text-small">&nbsp;
          </td>
        </tr>-->
  			<thead>
        <tr align="center">
          <th bgcolor="#BBDDFF">
            <b><!--<a href="$thisPageNav?sort_ports=name">-->Port Name<!--</a>--></b>
          </th>
          <th bgcolor="#BBDDFF">
            <b><!--<a href="$thisPageNav?sort_ports=port">-->Port<!--</a>--></b>
          </th>
          <th bgcolor="#BBDDFF">
            <b><!--<a href="$thisPageNav?sort_ports=group">-->Assigned Group<!--</a>--></b>
          </th>
          <th bgcolor="#BBDDFF" align="right" style="width: 140x;">
            <b>Control</b>&nbsp;&nbsp;&nbsp;
          </th>
        </tr>
				</thead>
$specific_port_list
      </table>
			</div>
EDIT_PORTS;
}
$htmlBody .= <<<MAIN_HTML
    </div>
	</div>
	<script>
		$(function() {
			$( "#setupTabs" ).tabs();
		});
	</script>

MAIN_HTML;

#####################################
## EXECUTE DISPLAY OF PRIMARY HTML ##
##
echo $htmlBody;
}
else {
	require_once ($site_abspath . '/header.tpl');
  display_login();
}

require_once ($site_abspath . '/footer.tpl');

?>
