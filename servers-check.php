<?php

//session_start();

require_once ('global.php');
if (isset($_GET['chkpass'])) $thisPage = $thisPage . '?chkpass=' . $_GET['chkpass'];
require_once ($site_abspath . '/header.tpl');

// Get arguments from the argv array
if (isset($_SERVER['argv'])){
	$arg1 = $_SERVER['argv'][1];
	$arg2 = $_SERVER['argv'][2];
	$arg3 = $_SERVER['argv'][3];
}

//if ($userinfo->userid != 0) {
//  if ($userinfo->admin_lvl >= 300) {

  if (isset($_GET['chkpass']) && hash('sha256', $_GET['chkpass']) == $check_password) {

    if (isset($arg2) && $arg2 == 'indv_thread') {
      $servers = NULL;
      $server_list = '';
      $servers = explode(',', $arg1);
      //$j = 0;
      //$tmp = '';
      $servers_checked = print_r($servers, true);
      //mail($mailAdmin, 'Thread Executed - ID: '. $arg3, 'You are receiving this email because a thread has just been launch with the following servers to process: ' . "\n\n" . $servers_checked, 'From: netMon <netmon@g3host.net>');
      if ($servers) {
        foreach ( $servers as $server )
        {
          check_server($server);
          //$tmp .= "\ncheck_server($server)";
        	$mtime = explode(' ', microtime());
        	$totaltime = $mtime[0] + $mtime[1] - $starttime;
        	if ($totaltime > $global_config->max_thread_exec_time) {
            mail($mailAdmin, 'Warning: Thread Halted!', "Warning: The thread with the following id [$arg3] has been halted after exceeding a max exec time of $global_config->max_thread_exec_time seconds.\n\n This could be caused by a large number of servers/ports being down. It can usually be fixed by bringing the servers back online or lowering the check timeout in the config file.\n\n Additionally, check to ensure the firewalls on the servers you are checking are not blocking the NetMon script server.\n\nDebug Data: $totaltime > $global_config->max_thread_exec_time", $mailFromHeader);
            exit;
        	}
        }
      }
      //mail($mailAdmin, 'Thread Completed - ID: ' . $arg3, 'The thread containing the following servers seems to have completed successfully:' . "\n\n" . $servers_checked . "\nTotal Time to Complete: $totaltime", 'From: raNetMon <netmon@g3host.net>');
    }
    else if ($global_config->use_threads) {
      $servers = NULL;
      $servers = $db->get_results("SELECT serverid FROM " . DB_TABLE_SERVERS);
      $num_of_servers = count($servers);

      if ($global_config->use_threads == 'threads') {  // threads based on servers per thread
        $servers_per_thread = $num_of_servers / $global_config->num_threads;
        $servers_per_thread = ceil($servers_per_thread);
        echo "<br>Set Number of Threads Method<br><br>num_of_servers: $num_of_servers | num_threads: $global_config->num_threads | servers_per_thread: $servers_per_thread<br><br>";
        $cur_thread_id = 1;
        $cur_sv_assigned = 0;
        $cur_sv_list[$cur_thread_id] = '';
        $servers_assigned = '';
        foreach ( $servers as $server ) {
          $cur_sv_list[$cur_thread_id] .= $comma . $server->serverid;
          $cur_sv_assigned++;
          $servers_assigned++;
          $comma = ',';
          if ($cur_sv_assigned >= $servers_per_thread || $servers_assigned >= $num_of_servers) {
            $timestamp_id = mktime();
            $cmd[$cur_thread_id] = "$global_config->path2php $site_abspath/check_servers.php $cur_sv_list[$cur_thread_id] indv_thread $timestamp_id-$cur_thread_id > /dev/null &";
            $cmd_explain[$cur_thread_id] = "Spawning Thread $cur_thread_id [$timestamp_id-$cur_thread_id] &rarr; Servers List :: $cur_sv_list[$cur_thread_id]<br>";
            echo $cmd_explain[$cur_thread_id];
            exec($cmd[$cur_thread_id]);
            $cur_sv_assigned = 0;
            $cur_thread_id++;
            $cmd[$cur_thread_id] = '';
            $comma = '';
          }
        }
      }
      else if ($global_config->use_threads == 'servers') {  // else, threads based on number of threads (default)
        $servers_per_thread = $global_config->max_per_thread;
        $num_threads = ceil($num_of_servers / $servers_per_thread);
        echo "<br>Max Servers Per Thread Method<br><br>num_of_servers: $num_of_servers | num_threads: $num_threads | servers_per_thread: $servers_per_thread<br><br>";
        $cur_thread_id = 1;
        $cur_sv_assigned = 0;
        $cur_sv_list[$cur_thread_id] = '';
        $servers_assigned = '';
        foreach ( $servers as $server ) {
          $timestamp_id = mktime();
          $cur_sv_list[$cur_thread_id] .= $comma . $server->serverid;
          $cur_sv_assigned++;
          $servers_assigned++;
          $comma = ',';
          if ($cur_sv_assigned >= $servers_per_thread || $servers_assigned >= $num_of_servers) {
            $cmd[$cur_thread_id] = "$global_config->path2php $site_abspath/check_servers.php $cur_sv_list[$cur_thread_id] indv_thread $timestamp_id-$cur_thread_id > /dev/null &";
            $cmd_explain[$cur_thread_id] = "Spawning Thread $cur_thread_id [$timestamp_id-$cur_thread_id] &rarr; Servers List :: $cur_sv_list[$cur_thread_id]<br>";
            echo $cmd_explain[$cur_thread_id];
            exec($cmd[$cur_thread_id]);
            $cur_sv_assigned = 0;
            $cur_thread_id++;
            $cmd[$cur_thread_id] = '';
            $comma = '';
          }
        }
      }
    }
    else {
      if (isset($arg2) && $arg2 == 'indv_thread')
        mail($mailAdmin, 'Warning', 'Warning: The main script executed during a thread and it should not have. Please report this to http://mantis.trinitywebs.com', 'From: raNetMon <raNetMon@g3host.net>');

      $check_allow = $db->get_var('SELECT config_value FROM ' . DB_TABLE_CONFIG . " WHERE config_name='check_allow'");
      //$check_allow = TRUE;

      if ($check_allow) {

        /* set check_allow to 0 to not allow this check program to run again until it has finished it's current cycle */
        $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='0' WHERE config_name='check_allow'");

        $servers = NULL;
        $server_list = '';
        $servers = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS);
        if ($servers) {
          foreach ( $servers as $server )
          {
            //$server_list .= $server->serverid . ' ' . $server->hostname . "<br>\n";
            check_server($server->serverid);
          }
        }

        /* set check_allow back to 1 to allow this script to run again now that this one has completed */
        $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='1' WHERE config_name='check_allow'");

        /* set check_attempts back to 0 since the check was just successfully run */
        $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='0' WHERE config_name='check_attempts'");

      	$mtime = explode(' ', microtime());
      	$totaltime = $mtime[0] + $mtime[1] - $starttime;
        $debug_vars = sprintf('[ Time: %.3fs | ' . $db->num_queries . ' Queries ]', $totaltime);
        //if ($totaltime > 100 && DEBUG)
          //mail($mailAdmin, 'Notice About NetMon', 'NetMon took over 300 seconds to execute.' . "\n\n$debug_vars", 'From: raNetMon <raNetMon@g3host.net>');

      }
      else {
        $check_attempts = $db->get_var('SELECT config_value FROM ' . DB_TABLE_CONFIG . " WHERE config_name='check_attempts'");
        if ($check_attempts == 1)
        {
          $check_attempts++;
          $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='$check_attempts' WHERE config_name='check_attempts'");
          //notify_msg('The last check servers script did not complete successfully the last time it was executed, there was probably a problem with the script. Check to make sure the script is still not trying to run or currently running and then reset the config value to allow the auto check servers script to run once again.', 'error');
          notify_msg('check_servers.php script has not yet completed running its checks or did not end correctly from the previous execution. <br><br>Check to make sure the script is not currently running and then reset the config value to allow the auto check servers script to run once again.<br><br>The config value will be reset automatically after 3 failed attempts.<br><br>Attempts since last run: ' . $check_attempts, 'error');
          mail($mailAdmin, 'Problem with NetMon', 'NetMon check servers script did not complete during last execution for unknown reasons. Due to this event, NetMon is automatically skipping the next two scheduled server checks and then resetting the check scripts error flag and attempting to run again on the 3rd try. This cycle will continue if check script continues generating errors. If that is the case, it needs to be looked into ASAP, if everything is fine then this should be the only e-mail you recieve about this error. However, the script should still be looked at because it should not be generating these errors ever, unless for some reason the script took an extremely long time to run.', $mailFromHeader);
        }
        else if ($check_attempts >= 3)
        {
          $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='1' WHERE config_name='check_allow'");
          $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='0' WHERE config_name='check_attempts'");
          //notify_msg('The last check servers script did not complete successfully the last time it was executed, there was probably a problem with the script. Check to make sure the script is still not trying to run or currently running and then reset the config value to allow the auto check servers script to run once again.<br><br>The run flag as just been reset.', 'error');
          notify_msg('After making a number of attempts to run the check_servers.php script, NetMon will now automatically reset the config value so that it may execute next time. These errors are often caused by a large number of downed servers.<br><br>Attempts since last run: ' . $check_attempts . '<br><br>Check allow flag resetting now.', 'error');
          mail($mailAdmin, 'NetMon Reset', 'As stated in a previous e-mail, the check servers script has just been automatically reset on this second scheduled check after the initial e-mail.' . "\n\n" . 'The script should run correctly at the next scheduled run, if not, you will likely see another error e-mail shortly.', $mailFromHeader);
        }
        else
        {
          $check_attempts++;
          $db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='$check_attempts' WHERE config_name='check_attempts'");
          notify_msg('check_servers.php script has not yet completed running its checks or did not end correctly from the previous execution. <br><br>Check to make sure the script is not currently running and then reset the config value to allow the auto check servers script to run once again.<br><br>The config value will be reset automatically after 3 failed attempts.<br><br>Attempts since last run: ' . $check_attempts, 'error');
        }
      }

      // These are only here for debugging, delete or comment them for the production version.
      // They are used to reset the check servers config in case there is a error during it's last run.
      //$db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='1' WHERE config_name='check_allow'");
      //$db->query('UPDATE ' . DB_TABLE_CONFIG . " SET config_value='0' WHERE config_name='check_attempts'");

    } // close 'if ($global_config->use_threads)'

  }
	else {
		notify_msg('You are not authenticated to run this script.', 'error');
	}
    //echo $server_list;

//  }
//}
//else {
//  display_login();
//}

require ($site_abspath . '/footer.tpl');

?>
