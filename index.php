<?php

session_start();

require_once ('global.php');
require_once ($site_abspath . '/header.tpl');

if (DEBUG){
	print '<p align="center"><a href="index.php">Reload</a></p>';
	print_r ($_SESSION);
}

if (!file_exists($site_abspath . '/install.php')){
	if (!$users_exist && !isset($_POST['username_new']) && !isset($_POST['password_new']) && !isset($_POST['confirm_new'])) {

		print <<<NEW_ADMIN
		  <br>
			<br>
		  <p align="center"><b><font size="4">netMon Installation</font></b></p>
		  <table width="500" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#444444">
		    <form name="new_admin" method="post" action="$thisPage">
		      <tr bgcolor="f0f0f0">
		        <td align="center">No users currently exist in database. Assuming new installation
		          please create your first administrative user.<br><br>
		          <table width="200" border="0" cellspacing="0" cellpadding="0">
		            <tr>
		              <td align="right">Username: </td>
		              <td align="left"> <input name="username_new" type="text" value="admin" size="24" maxlength="30"></td>
		            </tr>
		            <tr>
		              <td align="right">Email: </td>
		              <td align="left"> <input name="email_new" type="text" size="24" maxlength="50"></td>
		            </tr>
		            <tr>
		              <td align="right">Password: </td>
		              <td align="left"> <input name="password_new" type="password" size="24" maxlength="64"></td>
		            </tr>
		            <tr>
		              <td align="right">Confirm: </td>
		              <td align="left"> <input name="confirm_new" type="password" size="24" maxlength="64"></td>
		            </tr>
		          </table>
		          <br>
		          <input type="submit" name="CreateAdmin" value="Create Admin"> </td>
		      </tr>
		    </form>
		  </table>
NEW_ADMIN;
	}
	elseif (isset($userinfo) && $userinfo->userid != 0) {

		// Determin if $_GET['num_events'] is set and if it's an number, if so, make sure it's an integer and apply to variable.
		if (isset($_GET['num_events']))
		{
			if (is_numeric($_GET['num_events']))
			{
				intval($_GET['num_events']);
				$num_events = $_GET['num_events'];
			}
			else
			{
				$num_events = 10;
			}
		}
		else
		{
			$num_events = 10;
		}

	// this was commented out
		$status_list_servers = '<tr bgcolor="ffffff"><td align="center"><b>ID</b></td><td align="center"><b>Status</b></td><td align="center"><b>History</b></td><td align="center"><b>Hostname</b></td><td align="center"><b>IP Address</b></td></tr>' . "\n";

		if ($userinfo->admin_lvl < ADMINLVL_MASTER)
		{
			$sql = 'SELECT s.serverid, s.hostname, s.ip_address, s.current_status
				FROM ' . DB_TABLE_USER_GROUP . " ug, " . DB_TABLE_SERVER_GROUP . " sg, " . DB_TABLE_SERVERS . " s
				WHERE ug.userid = '$userinfo->userid'
					AND sg.groupid = ug.groupid
					AND s.serverid = sg.serverid";
			$servers = $db->get_results($sql);
		}
		else
		{
			$servers = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS);
		}

		if (!isset($_GET['log_only'])){
			if ($servers) {
				foreach ( $servers as $server )
				{
					switch ($server->current_status) {
						case 0:
							$status_image = IMAGE_NEUTRAL;
							break;
						case 1:
							$status_image = IMAGE_ONLINE;
							break;
						case 2:
							$status_image = IMAGE_OFFLINE;
							break;
						case 3:
							$status_image = IMAGE_CAUTION;
							break;
						case 4:
							$status_image = IMAGE_MAINT;
							break;
						default:
							$status_image = IMAGE_NEUTRAL;
					}

					$status_list_servers .= "<tr bgcolor=\"ffffff\"><td align=\"center\" class=\"padded\">$server->serverid</td><td align=\"center\" class=\"padded\"><img src=\"$images_url/$status_image\"></td><td align=\"center\" class=\"padded\"><a href=\"server-history.php?serverid=$server->serverid\" target=\"_blank\">Hist</a></td><td align=\"center\" class=\"padded\"><a href=\"server-edit.php?serverid=$server->serverid\">$server->hostname</a></td><td align=\"center\" class=\"padded\">$server->ip_address</td></tr>\n";
				}
			}
		}  // ended here


		if ($userinfo->admin_lvl < ADMINLVL_MASTER)
		{
			$sql = 'SELECT stat.*
				FROM ' . DB_TABLE_USER_GROUP . " ug, " . DB_TABLE_SERVER_GROUP . " sg, " . DB_TABLE_SERVERS . " s, " . DB_TABLE_STATUS_LOG . " stat
				WHERE ug.userid = '$userinfo->userid'
					AND sg.groupid = ug.groupid
					AND s.serverid = sg.serverid
					AND stat.serverid = s.serverid
					ORDER BY stat.time DESC LIMIT $num_events";
			$port_statuss = $db->get_results($sql);
		}
		else
		{
			$port_statuss = $db->get_results("SELECT * FROM " . DB_TABLE_STATUS_LOG . " ORDER BY time DESC LIMIT $num_events");
		}

		$status_list_ports = '<tr bgcolor="ffffff"><td align="center"><b>Status</b></td><td align="center"><b>Service</b></td><td align="center"><b>Hostname</b></td><td align="center"><b>IP Address</b></td><td align="center"><b>Date</b></td></tr>' . "\n";
		if ($port_statuss) {
			foreach ( $port_statuss as $port_status )
			{
				switch ($port_status->status) {
					case 0:
						$port_status_image = IMAGE_NEUTRAL;
						break;
					case 1:
						$port_status_image = IMAGE_ONLINE;
						break;
					case 2:
						$port_status_image = IMAGE_OFFLINE;
						break;
					case 3:
						$port_status_image = IMAGE_CAUTION;
						break;
					case 4:
						$port_status_image = IMAGE_MAINT;
						break;
					default:
						$port_status_image = IMAGE_NEUTRAL;
				}
				$port_status_time = getdate($port_status->time);
				if ($port_status_time['minutes'] <= 9)  $port_status_time['minutes'] = '0' . $port_status_time['minutes'];

				$port_status_time = $port_status_time['mon'] . '/' . $port_status_time['mday'] . ' ' . $port_status_time['hours'] . ':' . $port_status_time['minutes'];
				$port_status_server = $db->get_row("SELECT serverid, hostname, ip_address FROM " . DB_TABLE_SERVERS . " WHERE serverid='$port_status->serverid'", OBJECT);
				if ($port_status->portid != 0)
					$port_status_details = $db->get_row("SELECT portname, port FROM " . DB_TABLE_PORTS . " WHERE portid='$port_status->portid'", OBJECT);
				else {
					$port_status_details = new stdClass;
					$port_status_details->portname = 'Unknown';
					$port_status_details->port = 0;
				}
				//print_r($port_status);
				//print_r($port_status_server);
				//print_r($port_status_details);
				$status_list_ports .= '<tr bgcolor="ffffff"><td align="center" class="padded"><img src="' . $images_url . '/' . $port_status_image . '"></td><td align="center" class="padded">' . $port_status_details->portname . ' (' . $port_status_details->port . ')</td><td align="center" class="padded"><a href="server-edit.php?serverid=' . $port_status_server->serverid . '">' . $port_status_server->hostname . '</a></td><td align="center" class="padded">' . $port_status_server->ip_address . '</td><td align="center" class="padded">' . $port_status_time . '</td></tr>' . "\n";
			}
		}

		print <<<WELCOME
		  <br>
			<br>
			<table style="width:700px; padding: 5px; border-spacing: 0px; margin-left: auto; margin-right: auto; text-align: center; background-color: #f0f0f0; border: 2px solid #444444;">
				<tr>
					<td align="center">Good day $userinfo->username. Welcome to NetMon.<br><br>
					Please visit the setup page for futher settings. Below is last known statuses of all servers assigned to you.
					</td>
				</tr>
			</table>
WELCOME;

		// if "log_only" flag is enabled - do not show server summary, only recent activity log
		//if (!isset($_GET['log_only']))
			//display_server_summary_grid();

		print <<<WELCOME
			<br>
			<table width="680" cellpadding="2" cellspacing="0" border="1" bordercolor="#444444" style="margin-left: auto; margin-right: auto; text-align: center;">
			<tr bgcolor="ffffff"><td align="center" colspan="5" class="table-heading padded"><b>&nbsp;&nbsp;&nbsp;&nbsp;Servers Overview</b></td></tr>
			$status_list_servers
			</table>
			<br>
			<table width="600" cellpadding="2" cellspacing="0" border="1" bordercolor="#444444" style="margin-left: auto; margin-right: auto; text-align: center;">
			<tr bgcolor="ffffff"><td align="center" colspan="5" class="table-heading padded"><b>Most Recent Service Events</b></td></tr>
				$status_list_ports
				<form name="num_events" method="get" action="$thisPage">
				<tr bgcolor="ffffff"><td align="center" colspan="5">View Last:
					<select name="num_events">
						<option value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
						<option value="75">75</option>
						<option value="100">100</option>
						<option value="150">150</option>
						<option value="200">200</option>
						<option value="250">250</option>
						<option value="500">500</option>
						<option value="all">ALL</option>
					</select> Event Updates <input type="submit" value="Go">
				</td></tr>
				</form>
			</table>
WELCOME;
	}
	elseif (isset($_POST['CreateAdmin'])) {
		if ($_POST['password_new'] == $_POST['confirm_new']) {
			user_add($_POST['username_new'], $_POST['password_new'], $_POST['confirm_new'], $_POST['email_new'], 1, 500);
			display_login();
		}
		else {
			notify_msg('Failure: Passwords do not match.', 'error');
		}

	}
	else {
		display_login();
	}
}
else {
	echo '<br><br>';
	notify_msg('Remove install.php before proceeding!');
}

require ($site_abspath . "/footer.tpl");

?>
