#############################################################################################
The NetMon script is the property of Trinity Webs (www.trinitywebs.com) Copyright 2003-2015.
The source version may be modified by the purchaser. The script may not be resold whether
modified or not. The purchaser is free to use the script in any manner he sees fit, including
selling the abilities of the script as a service but limited to not selling the script itself
in any form.
#############################################################################################

NetMon Installation
===================

1. Upload all of the files to your server
2. Make config.php world writeable (chmod 777) so that install script can write to it unless you are using a configuation such as suPHP
3. Run install.php
4. After installation, remove install.php and set chmod on config.php to no longer writeable by world (but still readable - likely chmod 644)
5. Access index.php to create your first admin user
6. Set up your cron to run the script, you will use similar to the following commands (using the correct paths for your server):
*/10 * * * * apache /usr/bin/php /path/to/netmon/servers-check.php >> /dev/null 2>&1
*/30 * * * * apache /usr/bin/php /path/to/netmon/servers-refresh-ip.php >> /dev/null 2>&1

For security reasons, you may wish to set servers-check.php, servers-check-id.php & servers-refresh-ip.php chown and chmod as a user other than 'apache' and setup your cron to execute them using that user, however, this is not necessary.


If you have any problems you may contact netmon@trinitywebs.com
If you find any bugs, please report them to http://proj.trinitywebs.com
