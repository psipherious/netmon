define('DB_TABLE_CONFIG', 'config');

define('DB_TABLE_GROUPS', 'groups');

define('DB_TABLE_USERS', 'users');
--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `email_notify` tinyint(1) NOT NULL DEFAULT '0',
  `name_first` varchar(80) NOT NULL,
  `name_last` varchar(80) NOT NULL,
  `admin_lvl` smallint(5) unsigned NOT NULL DEFAULT '10',
  `icq` int(10) unsigned NOT NULL,
  `icq_notify` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(30) NOT NULL,
  `sms` varchar(20) NOT NULL,
  `sms_notify` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

define('DB_TABLE_USER_GROUP', 'user_group');

define('DB_TABLE_SERVERS', 'servers');
define('DB_TABLE_SERVER_GROUP', 'server_group');
define('DB_TABLE_SERVER_INDV_PORT', 'server_indv_port');
define('DB_TABLE_SERVER_PGROUP', 'server_pgroup');
define('DB_TABLE_SERVER_PORT', 'server_port');

define('DB_TABLE_PGROUPS', 'pgroups');
define('DB_TABLE_PORTS', 'ports');
define('DB_TABLE_PORT_GROUP', 'port_group');

define('DB_TABLE_LOG_OTHER', 'log_other');
define('DB_TABLE_STATUS_LOG', 'status_log');
define('DB_TABLE_SERVER_LOG', 'status_log_server');
define('DB_TABLE_HISTORIC_LOG', 'status_log_historic');