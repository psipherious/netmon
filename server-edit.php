<?php

session_start();

require_once ('./global.php');
$serverid = $_GET['serverid'];
$thisPage = $thisPage . "?serverid=$serverid";
require_once ($site_abspath . '/header.tpl');

if (!$serverid) {
  notify_msg('No server id specified, please return to setup page - program exiting ...', 'error');
	require_once ($site_abspath . '/footer.tpl');
  exit;
}

if ($userinfo->userid != 0) {

  $servergroup = $db->get_var("SELECT groupid FROM " . DB_TABLE_SERVER_GROUP . " WHERE serverid='$serverid'");
  $group_match = FALSE;
  if ($userinfo->usergroups) {
    foreach ($userinfo->usergroups as $usergroup) {
      if ($usergroup->groupid == $servergroup)
        $group_match = TRUE;
    }
  }

  if ($userinfo->admin_lvl >= 500 || $group_match == TRUE) {

    if ($serverid != 0) {

      // This is to add values to the variables below in case we're updatig the server, it uses those variables.
      $serverinfo = $db->get_row("SELECT * FROM " . DB_TABLE_SERVERS . " WHERE serverid='$serverid'");

      $host_name = isset($_POST['host_name']) ? $_POST['host_name'] : $serverinfo->hostname;
      $ip_address = isset($_POST['ip_address']) ? $_POST['ip_address'] : $serverinfo->ip_address;
      $server_name = isset($_POST['server_name']) ? $_POST['server_name'] : '';
      $company = isset($_POST['company']) ? $_POST['company'] : '';
      $server_info = isset($_POST['server_info']) ? $_POST['server_info'] : '';
      $maint_mode = isset($_POST['maint_mode']) ? $_POST['maint_mode'] : 0;
      $maint_checked = $maint_mode ? ' checked' : '';

      if (DEBUG){
        //print_r($userinfo);
        echo '<br><center><b>POST:</b> ';
        print_r($_POST);
        echo '<br><b>GET:</b> ';
        print_r($_GET);
        echo '</center><br>';
      }

      /* generate server groups list */
      $server_group_list = '';
      $server_groups = $db->get_results("SELECT groupid FROM " . DB_TABLE_SERVER_GROUP . " WHERE serverid='$serverid'");
      $add_comma = '';
      if ($server_groups) {
        foreach ( $server_groups as $server_group )
        {
          $current_server_group = $db->get_var("SELECT groupname FROM " . DB_TABLE_GROUPS . " WHERE groupid='$server_group->groupid'");
          $server_group_list .= $add_comma . $current_server_group;
          $add_comma = ', ';
        }
      }

      if (isset($_POST['action_update_server'])) {
        $actual_ip_address = gethostbyname($host_name . '.');
        //echo '<b>TEST: ' . $ip_address . '</b>';
        //first we need code to confirm the current logged in user has permission to update this server. If they are 500 they are an admin and can update it, if they are anything less then they must be checked for group permissions
        server_update($serverid, NULL, $host_name, $server_name, $company, $server_info, $maint_mode);
      }
      if (isset($_POST['submit_assign_server_pgroup'])) {
        server_assign_pgroup($serverid, $_POST['select_server_pgroup']);
      }
      if (isset($_POST['submit_assign_server_port'])) {
        server_assign_indv_port($serverid, $_POST['select_server_port']);
      }
      if (isset($_GET['removepgroup'])) {
        server_remove_pgroup($serverid, $_GET['removepgroup']);
      }
      if (isset($_GET['removeport'])) {
        server_remove_indv_port($serverid, $_GET['removeport']);
      }

      $serverinfo = $db->get_row("SELECT * FROM " . DB_TABLE_SERVERS . " WHERE serverid='$serverid'");

      //print_r($serverinfo);
      $host_name = isset($_POST['host_name']) ? $_POST['host_name'] : $serverinfo->hostname;
      $ip_address = isset($_POST['ip_address']) ? $_POST['ip_address'] : $serverinfo->ip_address;
      $server_name = isset($_POST['server_name']) ? $_POST['server_name'] : $serverinfo->servername;
      $company = isset($_POST['company']) ? $_POST['company'] : $serverinfo->company;
      $server_info = isset($_POST['server_info']) ? stripslashes($_POST['server_info']) : stripslashes($serverinfo->server_info);
      $maint_mode = isset($_POST['maint_mode']) ? $_POST['maint_mode'] : $serverinfo->maint_mode;
      $maint_checked = $maint_mode ? ' checked' : '';

      /* generate general port group list, used for select boxes */
      $pgroup_list = '';
      $pgroups = $db->get_results("SELECT * FROM " . DB_TABLE_PGROUPS);
      if ($pgroups) {
        foreach ( $pgroups as $pgroup )
        {
          $pgroup_list .= '<option value="' . $pgroup->pgroupid . '">' . $pgroup->pgroupname . '</option>' . "\n";
        }
      }

			// I assume this is to select displaying ports only from a specific group but this doesn't seem to be assigned
			// anywhere so I have added this "blank" assignment until proper assignment can be addeda at a later time
			$set_port_group = '';  // will just select all ports
      // generate the specific list of ports (specifc as in: with pgroup name and other info)
      $port_list = '';
      $specific_ports = $db->get_results("SELECT * FROM " . DB_TABLE_PORTS . $set_port_group . " ORDER BY port");
      if ($specific_ports) {
        foreach ( $specific_ports as $specific_port )
        {
          $port_list .= '<option value="' . $specific_port->portid . '">' . $specific_port->portname . " ($specific_port->port)" . '</option>' . "\n";
        }
      }

      /* generate server port group list */
      $add_comma = '';
      $server_pgroup_list = '';
      $server_pgroups = $db->get_results("SELECT * FROM " . DB_TABLE_SERVER_PGROUP . " WHERE serverid='" . $serverid . "'");
      if ($server_pgroups) {
        foreach ( $server_pgroups as $server_pgroup )
        {
          $curr_server_pgroup = $db->get_row("SELECT * FROM " . DB_TABLE_PGROUPS . " WHERE pgroupid='" . $server_pgroup->pgroupid . "'");
          //$server_pgroup_list .= $add_comma . $curr_server_pgroup->pgroupname;
          $server_pgroup_list .= $add_comma . '<span class="small_text">[<a href="javascript:confirm_link(&quot;' . $thisPage . '&removepgroup=' . $server_pgroup->pgroupid . '&quot;, &quot;Are you sure you wish to remove this port group?&quot;);">remove</a>]</span>->' . $curr_server_pgroup->pgroupname;
          $add_comma = ',<br>';
        }
      }

      //print_r($curr_server_pgroup);

      /* generate server port list */
      $add_comma = '';
      $formnum = 0;
      $server_port_list = '';
      $server_ports = $db->get_results("SELECT * FROM " . DB_TABLE_SERVER_INDV_PORT . " WHERE serverid='" . $serverid . "'");
      if ($server_ports) {
        foreach ( $server_ports as $server_port )
        {
          $formnum++;
          $curr_server_port = $db->get_row("SELECT * FROM " . DB_TABLE_PORTS . " WHERE portid='" . $server_port->portid . "'");
          $server_port_list .= $add_comma . '<span class="small_text">[<a href="javascript:confirm_link(&quot;' . $thisPage . '&removeport=' . $server_port->portid . '&quot;, &quot;Are you sure you wish to remove this port?&quot;);">remove</a>]</span>->' . $curr_server_port->portname . ' (' . $curr_server_port->port . ')';
          $add_comma = ',<br>';
        }
      }

      // Get the status of the ports for this server
      $port_statuss_status = $db->get_results("SELECT * FROM " . DB_TABLE_STATUS_LOG . " WHERE serverid='$serverid' ORDER BY time DESC LIMIT 10");
      $status_list_ports = '<tr bgcolor="ffffff"><td align="center"><b>Log ID</b></td><td align="center"><b>Status</b></td><td align="center"><b>Service</b></td><td align="center"><b>Hostname</b></td><td align="center"><b>IP Address</b></td><td align="center"><b>Date</b></td></tr>' . "\n";
      if ($port_statuss_status) {
        foreach ( $port_statuss_status as $port_status )
        {
          switch ($port_status->status) {
            case 0:
              $port_status_status_image = IMAGE_NEUTRAL;
              break;
            case 1:
              $port_status_status_image = IMAGE_ONLINE;
              break;
            case 2:
              $port_status_status_image = IMAGE_OFFLINE;
              break;
            case 3:
              $port_status_status_image = IMAGE_CAUTION;
              break;
            case 4:
              $port_status_status_image = IMAGE_MAINT;
              break;
            default:
              $port_status_status_image = IMAGE_NEUTRAL;
          }
          $port_status_time = getdate($port_status->time);
          if ($port_status_time['minutes'] <= 9)
            $port_status_time['minutes'] = '0' . $port_status_time['minutes'];
          $port_status_time = $port_status_time['mon'] . '/' . $port_status_time['mday'] . ' ' . $port_status_time['hours'] . ':' . $port_status_time['minutes'];
          $port_status_server = $db->get_row("SELECT hostname, ip_address FROM " . DB_TABLE_SERVERS . " WHERE serverid='$port_status->serverid'");
          $port_status_details = $db->get_row("SELECT portname, port FROM " . DB_TABLE_PORTS . " WHERE portid='$port_status->portid'");

          $status_list_ports .= '<tr bgcolor="ffffff"><td align="center">' . $port_status->statusid . '</td><td align="center"><img src="' . $images_url . '/' . $port_status_status_image . '" onmouseover="domTT_activate(this, event, \'caption\', \'Status Meanings\', \'content\', \'Grey - means never checked, deleted or maintenence mode at last check.<br><br>Legend incomplete.\', \'trail\', true, \'fade\', \'in\');"></td><td align="center">' . $port_status_details->portname . ' (' . $port_status_details->port . ')</td><td align="center">' . $port_status_server->hostname . '</td><td align="center">' . $port_status_server->ip_address . '</td><td align="center">' . $port_status_time . '</td></tr>' . "\n";
        }
      }


print <<<EDIT_SERVER
    <table class="sub-edit-section">
      <form name="server_info" method="post" action="$thisPage">
        <tr>
          <td align="center">Server Information<br> <br>
            <table class="sub-edit-box">
              <tr>
                <td align="right" valign="top"><b>Host Name:</b> </td>
                <td align="left"><input type="text" name="host_name" value="$host_name" size="20" maxlength="80"></td>
              </tr>
              <tr>
                <td align="right" valign="top"><b>IP Address:</b> </td>
                <td align="left">$ip_address</td>
              </tr>
              <tr>
                <td align="right" valign="top"><b>Server Name:</b> </td>
                <td align="left"><input type="text" name="server_name" value="$server_name" size="20" maxlength="50"></td>
              </tr>
              <tr>
                <td align="right" valign="top"><b>Company:</b> </td>
                <td align="left"><input type="text" name="company" value="$company" size="20" maxlength="50"></td>
              </tr>
              <tr>
                <td align="right" valign="top"><b>Server Info:</b> </td>
                <td align="left"><textarea name="server_info" value="$server_info" cols="20" rows="3">$server_info</textarea></td>
              </tr>
              <tr>
                <td align="right" valign="top"><b>Maintenance Mode:</b> </td>
                <td align="left"><input name="maint_mode" type="checkbox" id="maint_mode" value="1"$maint_checked></td>
              </tr>
              <tr>
                <td align="right" valign="top"><b>Group(s):</b> </td>
                <td align="left">
EDIT_SERVER;
                echo $server_group_list;
print <<<EDIT_SERVER
              </td>
              </tr>
            </table>
            <br>
            <input type="submit" name="submit" value="Update Server">
            <input type="hidden" name="action_update_server" value="true">
            <br>&nbsp;
          </td>
        </tr>
      </form>
      <tr>
        <td>
        <table class="sub-edit-box" style="width: 490px; border-spacing: 5px; border: 0;">
        <form name="assign_server_pgroup" method="post" action="$thisPage">
          <tr>
            <td align="center" valign="bottom">
              <select name="select_server_pgroup">
  $pgroup_list
              </select>
              &nbsp;
              <input type="submit" name="submit_assign_server_pgroup" value="Assign Port Group">
            </td>
          </tr>
        </form>
        <form name="assign_server_port" method="post" action="$thisPage">
          <tr>
            <td align="center" valign="bottom">
              <select name="select_server_port">
  $port_list
              </select>
              &nbsp;
              <input type="submit" name="submit_assign_server_port" value="Assign Individual Port">
            </td>
          </tr>
        </form>
          <tr>
            <td align="center" valign="bottom">
          <table width="360" border="0" align="center" cellpadding="0" cellspacing="2" bordercolor="#444444">
          <tr>
            <td align="right" valign="top" width="160"><b>Assigned Port Groups:&nbsp</b></td>
            <td align="left" valign="top">
EDIT_SERVER;
                  echo $server_pgroup_list;
print <<<EDIT_SERVER
            </td>
          </tr>
          <tr>
            <td align="right" valign="top" width="160"><b>Assigned Indv. Ports:&nbsp</b></td>
            <td align="left" valign="top">
EDIT_SERVER;
                  echo $server_port_list;
print <<<EDIT_SERVER
            </td>
          </tr>
          <tr>
            <td align="center" valign="top" colspan="2"><hr></td>
            </td>
          </tr>
          <tr>
            <td align="right" valign="top" width="160"><b>All Ports Combined:&nbsp</b></td>
            <td align="left" valign="top">
EDIT_SERVER;

      $set_server_port_list = get_full_port_list($serverid);

      /* in the two processes above, all the ports were merged into one multi array, print that array now */
      $add_comma = '';
      $arrayLength = count($set_server_port_list);
      for ($j = 0; $j < $arrayLength; $j++){
        //print_r ($set_server_port_list[$j]);
        print $add_comma . $set_server_port_list[$j]['name'] . ' (' . $set_server_port_list[$j]['port'] . ')';
        $add_comma = ', <br>';
        //echo '<br><br>';
      }

      if (DEBUG) {
        print '<br><br>Redundant Ports Assigned: ' . $multiple_ports;
      }

print <<<EDIT_SERVER
            </td>
          </tr>
          </table>
            </td>
          </tr>
        </table>

        <hr>
        <table width="100%" align="center" cellpadding="1" cellspacing="0" border="1" bordercolor="#444444">
          $status_list_ports
        </table>

        </td>
      </tr>
    </table>
EDIT_SERVER;

    }
    else {
      notify_msg('Something is wrong, I can not retrieve information for the server you are attempting to edit.', 'error');
    }
  }
  else {
    notify_msg('Your user does not have the authorization level to access this page.', 'error');
  }
}
else {
  display_login();
}

require ($site_abspath . "/footer.tpl");

?>
