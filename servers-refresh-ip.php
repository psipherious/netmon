<?php

  require ('global.php');
  $thispage = $_SERVER['SCRIPT_NAME'];
  require ($site_abspath . "/header.tpl");

  $server_list = '';
  $servers = $db->get_results("SELECT * FROM " . DB_TABLE_SERVERS);
  if ($servers)
  {
    foreach ( $servers as $server )
    {
      // This will double check the IP address to see if it has been updated.
      // If it has then it will update the database.

      $leading_space = '&nbsp;&nbsp;&nbsp;&nbsp;';

      $server_id = $server->serverid;
      $current_ip_address = gethostbyname($server->hostname . '.');
      $recorded_ip_address = $server->ip_address;
      //$\server->ip_address = $current_ip_address;
  
      echo '<b>' . $server->hostname . ':</b> ';
      if ( $current_ip_address != $recorded_ip_address )
      {
        $sql = 'UPDATE ' . DB_TABLE_SERVERS . " SET ip_address='$current_ip_address' WHERE serverid='$server_id'";
        $success = $db->query($sql);
        if ($success == true) {
          echo '<span style="color: #009900;">IP address changing from [' . $recorded_ip_address . '] to [' . $current_ip_address . ']. Database updated.</span><br /><br />';
          // Add code here to add a log to other events.
        }
      }
      else
      {
        echo 'IP remains the same. No changes.<br /><br />';
      }
    }
  }

  require ($site_abspath . "/footer.tpl");

?>
