<?php

$starttime = explode(' ', microtime());
$starttime = $starttime[1] + $starttime[0];

$thisPage = $_SERVER['SCRIPT_NAME'];
$thisPageNav = $thisPage;
$thisScript = $_SERVER['SCRIPT_NAME'];

require_once('config.php');
require_once($include_abspath . '/db_core.php'); // SQL functions shared core
require_once($include_abspath . '/db_mysqli.php'); // mySQL functions
//require($include_abspath . '/db_sqlite3.php'); // SQLite3 functions
require_once($include_abspath . '/functions.php');

// Before we do anything else, lets try a quick connection on the DB to make sure it even works
$dbTest = @mysqli_connect($dbhost, $dbuser, $dbpasswd, $dbname)
	or die(notify_msg_fullpage("I'm sorry but there was a problem connecting to your database, please check your configuration settings and/or check your database server status <br><br>Database error reported is: " . mysqli_connect_error(), 'error'));
mysqli_close($dbTest);

$db = new ezSQL_mysqli($dbuser, $dbpasswd, $dbname, $dbhost);  // mySQL connection
//$db=new ezSQL_sqlite3($site_abspath . '/db.sqlite');  // SQLite3 connection;
//$db->debug();
//$db->hide_errors();
$db->show_errors_custom();

set_error_handler('netmonErrorHandler', E_ALL);

define('DB_TABLE_CONFIG', 'config');

define('DB_TABLE_GROUPS', 'groups');

define('DB_TABLE_USERS', 'users');
define('DB_TABLE_USER_GROUP', 'user_group');

define('DB_TABLE_SERVERS', 'servers');                    // list of servers
define('DB_TABLE_SERVER_GROUP', 'server_group');          // groups of servers
define('DB_TABLE_SERVER_INDV_PORT', 'server_indv_port');  // individual port, not in a group, assigned to a server to be checked
define('DB_TABLE_SERVER_PGROUP', 'server_pgroup');        // port groups that are assigned to a server
define('DB_TABLE_SERVER_PORT', 'server_port');

define('DB_TABLE_PGROUPS', 'pgroups');
define('DB_TABLE_PORTS', 'ports');
define('DB_TABLE_PORT_GROUP', 'port_group');

define('DB_TABLE_LOG_OTHER', 'log_other');
define('DB_TABLE_STATUS_LOG', 'status_log');
define('DB_TABLE_SERVER_LOG', 'status_log_server');
define('DB_TABLE_HISTORIC_LOG', 'status_log_historic');

define('STATUS_NEUTRAL', 0);
define('STATUS_ONLINE', 1);
define('STATUS_OFFLINE', 2);
define('STATUS_CAUTION', 3);
define('STATUS_MAINT', 4);

define('ADMINLVL_MASTER', 500);
define('ADMINLVL_GROUP', 300);
define('ADMINLVL_USER', 10);

define('IMAGE_NEUTRAL', 'status_default.gif');
define('IMAGE_ONLINE', 'status_online.gif');
define('IMAGE_OFFLINE', 'status_offline.gif');
define('IMAGE_CAUTION', 'status_caution.gif');
define('IMAGE_MAINT', 'status_maintenance.gif');

/*
 * Initialize a few variables
 */
$notifyMsg = '';        // a blank string variable used to store notification messages to output in the header
$htmlBody = '';         // define initial variable for output of main html body
$jQueryFunctions = '';  // a blank string variable used to store jQuery functions to output in the footer

####################################
## User Authentication Process
##
if (isset($_SESSION['username_login']) && isset($_SESSION['password_login'])) {  // check if a session already exists
	$uName = $_SESSION['username_login'];
	$passwd = $_SESSION['password_login'];
  $userinfo = authenticate_user($uName, $passwd);
	unset($uName, $passwd);
}
else {
	$userinfo = new StdClass;
  $userinfo->userid = 0;
}
//echo '<pre>' . var_export($userinfo, true) . '</pre>';

// check if a user table exists in the database, if not, installer needs to be run, notify user.
$user_table_exists = $db->get_row("SHOW TABLES LIKE '" . DB_TABLE_USERS . "';");
//print_r($user_table_exists);
if (!$user_table_exists) {
	//notify_msg('It appears there is no user table in the database, you may need to <a href="install.php">run the installer</a>.', 'error');
	$notifyMsg = notify_msg('It appears there is no user table in the database, you may need to <a href="install.php">run the installer</a>.', 'error', true);
}
else {
	$users_exist = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_USERS . ';');  // check if any user at all exists in database
}

// decide whether user is logging in, logging out or creating the initial administrator
if (!isset($users_exist) && isset($_POST['username_new']) && isset($_POST['password_new']) && isset($_POST['confirm_new']) && isset($_POST['email_new'])) {
  user_add($_POST['username_new'], $_POST['password_new'], $_POST['confirm_new'], $_POST['email_new'], 1, 500);
}
elseif (isset($_GET['logout']) && $_GET['logout'] == true) {  // log out
  $_SESSION['username_login'] = NULL;
  $_SESSION['password_login'] = NULL;
  $userinfo = NULL;
  //session_destroy();
}
elseif (isset($_POST['username_login']) && isset($_POST['password_login'])) {
	$_SESSION['username_login'] = $_POST['username_login'];
	$_SESSION['password_login'] = hash('sha256', $_POST['password_login']);
	$uName = $_SESSION['username_login'];
	$passwd = $_SESSION['password_login'];
	$userinfo = authenticate_user($uName, $passwd);

  if ($userinfo==false || $userinfo->userid==0) {
		// user couldn't be authenticated so class needs to be initialized and set to 0 below otherwise we get "notices" throughout resst of script
		$userinfo = new StdClass;
		$userinfo->userid = 0;
    notify_msg('incorrect login', 'error');
  }
	unset($uName, $passwd);
}

?>
