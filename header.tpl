<?php

if (isset($userinfo) && $userinfo->userid != 0) {
  if ($userinfo->admin_lvl >= ADMINLVL_GROUP) {
    $linkSetupConfig = "<a href=\"$site_url/setup.php?nav=servers\">Setup & Config</a>";
	}
	else {
	  $linkSetupConfig = '';
	}
  $navbar = "<a href=\"$site_url/index.php\">Summary Grids</a> | $linkSetupConfig";
  $logInOut = "<a href=\"$site_url/user-edit.php?userid=$userinfo->userid\">$userinfo->username</a> | <a href=\"$site_url/index.php?logout=true\">Logout</a>";
}
else {
	$userinfo = new stdClass;
	$userinfo->userid = 0;
	$userinfo->username = 'Not Logged In';
  $logInOut = 'Not Logged In';
  $navbar = "<a href=\"$site_url/index.php\">Return Home</a>";
}

print <<<HTML_HEADER
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>netMon by Trinity Webs</title>
	<!-- ###### A Short Note From the Original Developer ######
		Viewing source in Google Chrome you can adjust tab size display by following method,
		create an empty	bookmark on your Bookmarks bar and insert the following line to the "location" box:
javascript:(function(){for(var i=0,l=document.all.length;i<l;++i)document.all[i].style.tabSize=2;}());
		Hit it and it will shink the viewing tab size down to '2' (you can set the size to your preference)
	-->
  <meta name="description" content="netMon: The network service monitoring tool.">
  <meta name="keywords" content="network monitoring, service monitoring, server monitoring, tool, script">
  <meta http-equiv="Pragma" CONTENT="no-cache"> 
  <meta http-equiv="Expires" CONTENT="-1">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="$site_url/inc/powertip.css" />
	<script type="text/javascript" src="$include_url/js/jquery.powertip.min.js"></script>
  <link rel="stylesheet" type="text/css" href="$site_url/netmon.css" />
</head>

<body bgcolor="#ffffff">
<div id="container">
	<!--
	<div id="header">
		<span style="font-size: 24px; font-weight: bold;">netMon:</span> The service monitoring tool
	</div>
	-->
	<div id="nav">
		<span class="navItem" style="float: left; width: 100px; text-align: left;"><a href="$thisPage">Reload</a></span>
		<span class="navItem" style="margin-left: auto; margin-right: auto; text-align: center;">$navbar</span>
		<span class="navItem" style="float: right; width: 100px; text-align: right;">$logInOut</a></span>
	</div>
	$notifyMsg

HTML_HEADER;

?>
