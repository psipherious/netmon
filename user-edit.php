<?php

session_start();

require_once ('./global.php');
if(isset($_GET['userid'])) {
	$userID = $_GET['userid'];
	$thisPage .= '?userid=' . $userID;
}
require_once ($site_abspath . '/header.tpl');

if (DEBUG){
  print '<br><center><a href="index.php">Reload</a><center>';
  print_r ($_SESSION);
}

if ($userinfo->userid != 0 && isset($_GET['userid']) && $_GET['userid'] != 0) {

  if (DEBUG){
    echo '<br><center>';
    print_r($userinfo);
    echo '<br>';
    print_r($_POST);
    echo '<br>';
    print_r($_GET);
    echo '</center><br>';
  }

  $sql = 'SELECT * FROM ' . DB_TABLE_USERS . " WHERE userid='" . $userID . "'";  // get userid and adminlvl
  $edituserinfo = $db->get_row($sql);
	//echo 'edituserinfo: ' . var_export($edituserinfo, true);
  
  $sql = 'SELECT groupid FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='" . $edituserinfo->userid . "'";  // get groups assigned to userid
  $userinfo->usergroups = $db->get_results($sql);

	// If current user is an admin OR user is editing their own account, allow edit to proceed
  if ($userinfo->admin_lvl >= ADMINLVL_MASTER || $userinfo->userid == $edituserinfo->userid) {
		if (isset($_POST['userid'])) {
			$sql = 'SELECT * FROM ' . DB_TABLE_USERS . " WHERE userid='" . $_POST['userid'] . "'";   // get userinfo to edit
			$edituserinfo = $db->get_row($sql);
			
			//echo 'edituserinfo: ' . var_export($_POST, true);
			if ($_POST['submit'] == 'Update Account') {
				if(!isset($_POST['email_notify'])) $_POST['email_notify'] = 0;
				if(!isset($_POST['aim_notify'])) $_POST['aim_notify'] = 0;
				if(!isset($_POST['sms_notify'])) $_POST['sms_notify'] = 0;
				user_update($edituserinfo->userid, $_POST['username'], $_POST['password'], $_POST['pwconfirm'], $_POST['email'], $_POST['email_notify'], $_POST['firstname'], $_POST['lastname'], $_POST['select_admin_level'], $_POST['aim'], $_POST['aim_notify'], $_POST['phone'], $_POST['sms'], $_POST['sms_notify']);
			}
			$sql = 'SELECT * FROM ' . DB_TABLE_USERS . " WHERE userid='" . $_POST['userid'] . "'";   // get info again from changed/updated info
			$edituserinfo = $db->get_row($sql);
		}
    //notify_msg('Something went haywire! You are now editing yourself.', 'error');
		//$edituserinfo = $userinfo;
	}
  else {
    notify_msg('You do not have permission to edit this user, instead your own info is being displayed.', 'error');
    $edituserinfo = $userinfo;
  }

  $username = isset($_POST['username']) ? $_POST['username'] : $edituserinfo->username;
  $email = isset($_POST['email']) ? $_POST['email'] : $edituserinfo->email;
  $email_notify = isset($_POST['email_notify']) ? $_POST['email_notify'] : $edituserinfo->email_notify;
  $email_checked = $email_notify ? ' checked' : '';
  $name_first = isset($_POST['firstname']) ? $_POST['firstname'] : $edituserinfo->name_first;
  $name_last = isset($_POST['lastname']) ? $_POST['lastname'] : $edituserinfo->name_last;
  $aim = isset($_POST['aim']) ? $_POST['aim'] : $edituserinfo->aim;
  $aim_notify = isset($_POST['aim_notify']) ? $_POST['aim_notify'] : $edituserinfo->aim_notify;
  $aim_checked = $aim_notify ? ' checked' : '';
  $phone = isset($_POST['phone']) ? $_POST['phone'] : $edituserinfo->phone;
  $sms = isset($_POST['sms']) ? $_POST['sms'] : $edituserinfo->sms;
  $sms_notify = isset($_POST['sms_notify']) ? $_POST['sms_notify'] : $edituserinfo->sms_notify;
  $sms_checked = $sms_notify ? ' checked' : '';

  //notify_msg('Currently only master admins and individual users can edit their own accounts. Group admin functionality will be added later.', 'notify');

	// default unselected admin-level
	$aLvlMasterSelected = '';	$aLvlGrpSelected = ''; $aLvlRegSelected = '';
	if ($edituserinfo->admin_lvl >= ADMINLVL_MASTER) {
		$admin_lvl_text = "Super Admin";
		$aLvlMasterSelected = ' selected';
	}
	else if ($edituserinfo->admin_lvl >= ADMINLVL_GROUP) {
		$admin_lvl_text = "Group Admin";
		$aLvlGrpSelected = ' selected';
	}
	else if ($edituserinfo->admin_lvl >= ADMINLVL_USER) {
		$admin_lvl_text = "Standard User";
		$aLvlRegSelected = ' selected';
	}

	if ($userinfo->admin_lvl >= ADMINLVL_MASTER) {
		$adminLvlSelector = '
			<option value="10"'. $aLvlRegSelected .'>Regular User</option>
			<option value="300"'. $aLvlGrpSelected .'>Group Admin</option>
			<option value="500"'. $aLvlMasterSelected .'>Super Admin</option>
										';
	}
	elseif ($userinfo->admin_lvl >= ADMINLVL_GROUP) {
		$adminLvlSelector = '
			<option value="10"'. $aLvlRegSelected .'>Regular User</option>
			<option value="300"'. $aLvlGrpSelected .'>Group Admin</option>
										';
	}
	else {
		$adminLvlSelections = '
			<option value="10" selected>Regular User</option>
										';
	}
	
print <<<EDIT_USER
    <table class="sub-edit-section">
      <form name="user_info" method="post" action="$thisPage">
        <tr> 
          <td align="center">Edit User Account<br>
            <br> 
            <table class="sub-edit-box" style="width: 300px; border-spacing: 4px;">
              <tr> 
                <td align="right">Admin Level: </td>
                <td align="left">
                    <select name="select_admin_level">
                    $adminLvlSelector
                    </select>
                </td>
              </tr>
              <tr> 
                <td align="right">Username: </td>
                <td align="left">
                  <input type="text" name="username" id="username" value="$username" size="20" maxlength="20">
                  <input type="hidden" name="userid" value="$edituserinfo->userid">
                </td>
              </tr>
              <tr> 
                <td align="right">Email: </td>
                <td align="left"><input type="text" name="email" id="email" value="$email" size="20" maxlength="50"> 
                  <input name="email_notify" type="checkbox" id="email_notify" value="1"$email_checked></td>
              </tr>
              <tr> 
                <td align="right">Password: </td>
                <td align="left"> <input type="password" name="password" id="password" size="20" maxlength="20">
                  &nbsp;<img src="$images_url/question_mark.jpg" class="tip" title="Only enter a password if you wish to update it."></td>
              </tr>
              <tr> 
                <td align="right">Confirm: </td>
                <td align="left"><input type="password" name="pwconfirm" id="pwconfirm" size="20" maxlength="20"></td>
              </tr>
              <tr> 
                <td align="right">First Name: </td>
                <td align="left"><input type="text" name="firstname" id="firstname" value="$name_first" size="20" maxlength="20"></td>
              </tr>
              <tr> 
                <td align="right">Last Name: </td>
                <td align="left"><input type="text" name="lastname" id="lastname" value="$name_last" size="20" maxlength="20"></td>
              </tr>
<!--
              <tr> 
                <td align="right">ICQ: </td>
                <td align="left"><input name="icq" type="text" id="icq" value=" icq" size="20" maxlength="20"> 
                  <input type="checkbox" name="icq_notify" id="icq_notify" value="1" icq_checked></td>
              </tr>
-->
              <tr> 
                <td align="right">AIM: </td>
                <td align="left"><input name="aim" type="text" id="aim" value="$aim" size="20" maxlength="20"> 
                  <input type="checkbox" name="aim_notify" id="aim_notify" value="1"$aim_checked> Unavail</td>
              </tr>
              <tr> 
                <td align="right">Phone: </td>
                <td align="left"><input type="text" name="phone" id="phone" value="$phone" size="20" maxlength="20">
                  &nbsp;<img src="$images_url/question_mark.png" height="12" width="12" class="tip" title="Contact number for your admin to call you."></td>
              </tr>
              <tr>
                <td align="right">SMS: </td>
                <td align="left"><input type="text" name="sms" id="sms" value="$sms" size="20" maxlength="50">
                  <input type="checkbox" name="sms_notify" id="sms_notify" value="1"$sms_checked> Unavail</td>
              </tr>
            </table>
            <br> <input type="submit" name="submit" value="Update Account"> </td>
        </tr>
      </form>
    </table>
EDIT_USER;
    
}
elseif ($userinfo->userid != 0 && isset($_GET['userid']) && $_GET['userid'] == 0) {
	notify_msg('You can not edit user 0', 'error');
}
else {
  display_login();
}

require_once ($site_abspath . "/footer.tpl");

?>
