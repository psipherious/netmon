<?php

session_start();

require_once ('./global.php');
$thisPage = $thisPage . '?serverid=' . $_GET['serverid'];
require_once ($site_abspath . '/header.tpl');

if ($userinfo->userid != 0) {

  if ($_GET['serverid']){
    $sql = 'SELECT `year_month`, `online`, `offline`, `caution`, `maint` FROM `status_log_historic` WHERE serverid=' . $_GET['serverid'] . ' ORDER BY `year_month` DESC';
    $server_hist_stats = $db->get_results($sql);
  }

  //print_r($server_hist_stats);

  if ($server_hist_stats) {

  $month_full = array(NULL, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

  $server_id = $_GET['serverid'];
  $current_server = $db->get_row("SELECT * FROM " . DB_TABLE_SERVERS . " WHERE serverid='$server_id'");
  $server_title = sprintf('[id: ' . $current_server->serverid . '] ' . $current_server->hostname . ' (' . $current_server->ip_address . ')');
  //echo $server_title;

print <<<HTML
<br />
<table class="sub-section" style="border: 0;">
  <tr>
    <td align="center">
      <table border="1" bordercolor="#999999" class="sub-section" style="border: 0;">
        <tr align="center" bgcolor="#555555">
          <td colspan="6" class="heading-medium">
            <font color="#FFFFFF">Historical Stats for &gt; $server_title</font>
          </td>
        </tr>
        <tr bgcolor="#555555">
          <td align="left" class="heading-small">
            <font color="#FFFFFF"><strong>Year</strong></font>
          </td>
          <td align="left" class="heading-small">
            <font color="#FFFFFF"><strong>Month</strong></font>
          </td>
          <td align="right" class="heading-small">
            <font color="#FFFFFF"><strong>Maint</strong></font>
          </td>
          <td align="right" class="heading-small">
            <font color="#FFFFFF"><strong>Caution</strong></font>
          </td>
          <td align="right" class="heading-small">
            <font color="#FFFFFF"><strong>Offline</strong></font>
          </td>
          <td align="right" class="heading-small">
            <font color="#FFFFFF"><strong>Online</strong></font>
          </td>
        </tr>
HTML;

    foreach ( $server_hist_stats as $server_hist_stat )
    {
      $curr_year = substr($server_hist_stat->year_month, 0, 4);
      $curr_month = substr($server_hist_stat->year_month, 4);
      $curr_month = $month_full[intval($curr_month)];
      //print_r($month_full);
      $total_checks = $server_hist_stat->online + $server_hist_stat->offline + $server_hist_stat->caution + $server_hist_stat->maint;
      $curr_online = sprintf('%.3f', $server_hist_stat->online / $total_checks * 100);
      $curr_offline = sprintf('%.3f', $server_hist_stat->offline / $total_checks * 100);
      $curr_caution = sprintf('%.3f', $server_hist_stat->caution / $total_checks * 100);
      $curr_maint = sprintf('%.3f', $server_hist_stat->maint / $total_checks * 100);

print <<<HTML
        <tr><td align="left" class="padded">$curr_year</td><td align="left" class="padded">$curr_month</td><td align="right" class="padded">$curr_maint%</td><td align="right" class="padded">$curr_caution%</td><td align="right" class="padded">$curr_offline%</td><td align="right" class="padded">$curr_online%</td></tr>
HTML;

      /*echo '<br />Year: ' . $curr_year . ' Month: ' . $curr_month;
      printf('<br />Online: %.3f', $server_hist_stat->online / $total_checks * 100);
      printf('<br />Offline: %.3f', $server_hist_stat->offline / $total_checks * 100);
      printf('<br />Caution: %.3f', $server_hist_stat->caution / $total_checks * 100);
      printf('<br />Maint: %.3f<br />', $server_hist_stat->maint / $total_checks * 100);*/
    }
print <<<HTML
      </table>
    </td>
  </tr>
</table>
HTML;
  }
  else
    echo '<br /><strong>No results could be found</strong>';

}
else {
  display_login();
}

require_once ($site_abspath . '/footer.tpl');

?>
