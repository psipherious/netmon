<?php

if (DEBUG) {
	$mtime = explode(' ', microtime());
	$totaltime = $mtime[0] + $mtime[1] - $starttime;
  $debug_vars = sprintf('<br />[ Time: %.3fs | ' . $db->num_queries . ' Queries ]', $totaltime);
}
else
	$debug_vars = '';

print <<<FOOTER
	<div id="debug" style="position: relative; width: 100%; margin-left: auto; margin-right: auto; background-color: #ffffff;">$debug_vars</div>
</div>
<br />
<br />
	<script language="javascript">
		function confirm_link(\$location, \$message) {
			 if (window.confirm(\$message)) {
					window.location.href = \$location;
			 }
		}
  </script>	
	<script type="text/javascript">
		$(function() {
			// PowerTip tooltip options
			$('.tip').powerTip({
				placement: 'e',
				smartPlacement: true,
			});
		});
$jQueryFunctions
	</script>
</body>
</html>
FOOTER;

?>
