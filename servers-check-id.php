<?php

require ('global.php');
$thispage = $_SERVER['SCRIPT_NAME'] . '?chkpass=' . $_GET['chkpass'] . '&serverid=' . $_GET['serverid'];
require ($site_abspath . "/header.tpl");

if (isset($_GET['chkpass']) && hash('sha256', $_GET['chkpass']) == $check_password) {
	if (isset($_GET['serverid'])) {
		$serverList = explode(',', $_GET['serverid']);
		foreach ( $serverList as $server ) {
			check_server($server);
		}
	}
	else
		echo 'no server specified';
}
else
	notify_msg('You are not authenticated to run this script.', 'error');

require ($site_abspath . "/footer.tpl");

?>
