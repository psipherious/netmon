<?php

//require ('global.php');

define('DEBUG', false);
define('ADMIN_DEBUG', false);

if (isset($_POST['InstallNow'])) {
	$thisPage = $_SERVER['SCRIPT_NAME'];
	$site_abspath      = $_POST['abs_path'];
	$include_abspath   = $site_abspath . "/inc";
	$images_abspath    = $site_abspath . "/media";
	$site_url          = $_POST['relative_path'];
	$include_url       = $site_url . "/inc";
	$images_url        = $site_url . "/media";
	if (isset($site_abspath) && isset($site_url)) {
		$notifyMsg = '';  // a blank string variable used to store notification messages to output in the header
		$jQueryFunctions = '';  // a blank string variable used to store jQuery functions to output in the footer
		require_once($site_abspath . '/header.tpl');
	}

	//print_r($_POST);

	$dbhost = $_POST['db_host'];
	$dbuser = $_POST['db_user'];
	$dbpasswd = $_POST['db_password'];
	$dbname = $_POST['db_name'];
	if (isset($_POST['db_port'])) $dbport = $_POST['db_port'];


/***************************************/
/* Following is the Configuration File */
/***************************************/
$config_file = '
<?php

define(\'DEBUG\', false);
define(\'ADMIN_DEBUG\', false);

/* database settings */
$dbhost = \'' . $dbhost . '\';
$dbport = \'' . $dbport . '\';
$dbuser = \'' . $dbuser . '\';
$dbpasswd = \'' . $dbpasswd . '\';
$dbname = \'' . $dbname . '\';

/* not yet implemented
$table_prefix = \'\';
*/

/* use an md5 hash here */
$check_password = \'\';

/* admin email, for check script and other such errors */
$admin_email = \'' . $_POST['admin_email'] . '\';

/* global config settings */
$global_config = new StdClass;
$global_config->chk_timeout = ' . $_POST['global_timeout'] . ';
$global_config->chk_timeout_secondary = ' . $_POST['global_timeout_secondary'] . ';
$global_config->use_threads = false;  // possible options: servers, threads, true, false
$global_config->num_threads = 5;
$global_config->max_per_thread = 5;
$global_config->max_thread_exec_time = 360;    // maximum amount of time a thread should be allowed before it is halted (in seconds - 300s = 5 min)
$global_config->path2php = \'/usr/bin/php\';

/* the absolute paths of all the sections so they can be read from anywhere */
$site_abspath      = "' . $_POST['abs_path'] . '";
$include_abspath   = $site_abspath . "/inc";
$images_abspath    = $site_abspath . "/media";

/* The resource paths of all the sections so they can be read from anywhere */
$site_url       = "' . $_POST['relative_path'] . '";
$include_url    = $site_url . "/inc";
$images_url     = $site_url . "/media";

?>
';

}  // end check for install command

/****************************************/
/* Following is the Database Setup Code */
/****************************************/
$install_sql = <<<INSTALL_SQL
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `config_name` varchar(255) NOT NULL DEFAULT '',
  `config_value` varchar(255) NOT NULL DEFAULT '',
  KEY `config_name` (`config_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `config` (`config_name`, `config_value`) VALUES
('check_allow', '1'),
('check_attempts', '0');


DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `groupid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `log_other`;
CREATE TABLE IF NOT EXISTS `log_other` (
  `logid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverid` int(8) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `logno` smallint(4) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `value1` varchar(254) NOT NULL DEFAULT '',
  `value2` varchar(254) NOT NULL DEFAULT '',
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `pgroups`;
CREATE TABLE IF NOT EXISTS `pgroups` (
  `pgroupid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pgroupname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pgroupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `ports`;
CREATE TABLE IF NOT EXISTS `ports` (
  `portid` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `port` mediumint(6) NOT NULL DEFAULT '0',
  `portname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`portid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `port_group`;
CREATE TABLE IF NOT EXISTS `port_group` (
  `portid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pgroupid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `portid` (`portid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `servers`;
CREATE TABLE IF NOT EXISTS `servers` (
  `serverid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `servername` varchar(50) NOT NULL DEFAULT '',
  `hostname` varchar(80) NOT NULL DEFAULT '',
  `ip_address` varchar(30) NOT NULL DEFAULT '',
  `company` varchar(50) NOT NULL DEFAULT '',
  `server_info` text,
  `current_status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `maint_mode` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`serverid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `server_group`;
CREATE TABLE IF NOT EXISTS `server_group` (
  `serverid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `groupid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `serverid` (`serverid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `server_indv_port`;
CREATE TABLE IF NOT EXISTS `server_indv_port` (
  `serverid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `portid` mediumint(6) unsigned NOT NULL DEFAULT '0',
  KEY `serverid` (`serverid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `server_pgroup`;
CREATE TABLE IF NOT EXISTS `server_pgroup` (
  `serverid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pgroupid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `serverid` (`serverid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `server_port`;
CREATE TABLE IF NOT EXISTS `server_port` (
  `serverid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `portid` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `current_status` tinyint(2) NOT NULL DEFAULT '0',
  KEY `serverid` (`serverid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `status_log`;
CREATE TABLE IF NOT EXISTS `status_log` (
  `statusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `portid` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `errcode` smallint(5) unsigned NOT NULL DEFAULT '0',
  `error` varchar(255) DEFAULT NULL,
  `dataread` varchar(255) DEFAULT NULL,
  `numchecks` mediumint(6) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`statusid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `status_log_historic`;
CREATE TABLE IF NOT EXISTS `status_log_historic` (
  `statusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverid` int(8) unsigned NOT NULL DEFAULT '0',
  `year_month` varchar(10) DEFAULT '0',
  `default` int(10) unsigned NOT NULL DEFAULT '0',
  `online` int(10) unsigned NOT NULL DEFAULT '0',
  `offline` int(10) unsigned NOT NULL DEFAULT '0',
  `caution` int(10) unsigned NOT NULL DEFAULT '0',
  `maint` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`statusid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `status_log_server`;
CREATE TABLE IF NOT EXISTS `status_log_server` (
  `statusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serverid` int(8) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`statusid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `userid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `email_notify` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(64) NOT NULL DEFAULT '',
  `admin_lvl` smallint(3) NOT NULL DEFAULT '0',
  `date_register` int(10) unsigned NOT NULL DEFAULT '0',
  `date_mod` int(10) unsigned NOT NULL DEFAULT '0',
  `name_first` varchar(50) NOT NULL DEFAULT '',
  `name_last` varchar(50) NOT NULL DEFAULT '',
  `aim` varchar(20) NOT NULL DEFAULT '',
  `aim_notify` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(20) NOT NULL DEFAULT '###-###-####',
  `sms` varchar(50) NOT NULL DEFAULT '###-###-####',
  `sms_notify` tinyint(1) NOT NULL DEFAULT '0',
  `notice_max` smallint(3) unsigned NOT NULL DEFAULT '10',
  `notices_sent` smallint(3) unsigned NOT NULL DEFAULT '0',
  `date_last_notice` mediumint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `groupid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSTALL_SQL;

if (isset($_POST['InstallNow'])) {

	$config_file = str_replace("\r\n", "\n", $config_file);
	//$config_file = str_replace("\n", "<br>", $config_file);

	echo '<div class="section">';

	/* Test database connection */
	echo '<br><b>Testing database connection...</b><br>';
	$testdb = @mysqli_connect($dbhost, $dbuser, $dbpasswd, $dbname)
	  or die("<br><b>Database connection failed: " . mysql_error() . "<br><br>Please use your browsers back button, or the Reload link above to re-enter your database info.</b>");
	echo "Connection established.";
	mysqli_close($testdb);

	echo '<br><br><b>Attempting database setup and installation...</b><br>';

	/* Begin database installation/setup */
	$sql_lines = explode(";", $install_sql);

	require_once ($include_abspath . '/db_core.php');
	require_once ($include_abspath . '/db_mysqli.php');

	// database connection variables are defined higher up
	$dbinstall = new ezSQL_mysqli($dbuser, $dbpasswd, $dbname, $dbhost);  // mySQL connection
	//$dbinstall=new db;
	//$dbinstall = mysqli_connect($dbhost, $dbuser, $dbpasswd, $dbname);

	if ($sql_lines) {
		//echo '<pre>' . var_export($sql_lines, true) .  '</pre>';
	  foreach ( $sql_lines as $sql_line )
	  {
	    if ($sql_line)  // make sure we're not going to try to run a blank line
	    {
				//$dbinstall->show_errors();
				//$dbinstall->show_errors_custom();
				$dbinstall->query($sql_line);
				//$dbinstall->debug();
	    }
			//else
				//die("blank line, zomg!");
	  }
	  echo "Import appears successful. Any previous data has been dropped. Database is setup.\n<br>";
		//mysqli_close($dbinstall);
	}

	echo '<br><b>Attempting to create configuration file...</b><br>';

	$config_filename = $site_abspath . '/config.php';

	// Make sure the file exists and is writable first.
	if (is_writable($config_filename)) {
	  if (!$fp_config = fopen($config_filename, 'w')) {
	       echo "Cannot open file ($config_filename).<br>";
	       exit;
	  }

	  // Write $config_file to our opened file.
	  if (!fwrite($fp_config, $config_file)) {
	     echo "Cannot write to file ($config_filename).<br>";
	     exit;
	  }

	  echo "Success, wrote configuration data to file ($config_filename).<br>";

	  fclose($fp_config);
	}
	else {
	  echo "The file $config_filename is not writable. Generating instructions for manual install.<br>";
	  echo '<br><br><strong>Below is your configuration file for NetMon.<br><br>
	    1. Select and copy all of the text between the two horizontal lines and paste it into a blank text file named config.php.<br><br>
	    2. Ensure that config.php is placed in the root directory with the rest of the NetMon and upload to your server.<br><br>
	    3. Delete install.php from the server.</strong><br>';
	  echo '<br><hr color="#000099">';
	  echo '<pre>' . htmlspecialchars($config_file) . '</pre>';
	  echo '<hr color="#000099"><br>';
	}

	//$fp_config = fopen($site_abspath . '/config.php', 'w');

	echo '<br><b>Once the above steps are complete, you may proceed to NetMon to create your Admin User.</b><br>
	  Note: You may wish to delete hashgen.php from the server for security reasons; however, it is not necessary.<br><br>
	  <b><a href="index.php">Proceed to NetMon</a></b><br><br>';

	echo '</div>';
}
else {

	$abspath = str_ireplace("\\", "/" , __FILE__);
	//$abspath = ereg_replace("\\\\", "/" , __FILE__);
	$abspath = dirname($abspath);
	$relpath = $_SERVER['HTTP_HOST'];
	if (dirname($_SERVER['SCRIPT_NAME'])!='/')
	  $relpath .= dirname($_SERVER['SCRIPT_NAME']);
	$relpath = "http://" . $relpath;

	echo <<<INSTALL_FORM
	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
	<head>
	<title>Setup Page</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="netmon.css" rel="stylesheet" type="text/css">
	<style type="text/css">
	<!--
	-->
	</style>
	</head>
	<body>
	<p align="center"><strong><font size="4">netMon Installation</font></strong></p>
	<table width="500" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#444444" style="margin-left: auto; margin-right: auto;">
	  <form name="install" method="post" action="$_SERVER[SCRIPT_NAME]">
	    <tr bgcolor="f0f0f0">
	      <td align="center">
	        <br>
	        <br>

	    <table width="400" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td>&nbsp;
	        </td>
	        <td align="left">
	          <strong>Database Setup</strong></td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          DB Host:
	        </td>
	        <td align="left">
	          <input name="db_host" type="text" id="db_host" value="localhost" size="24" maxlength="50">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          DB Port:
	        </td>
	        <td align="left">
	          <input name="db_port" type="text" id="db_port" value="3306" size="10" maxlength="10">
	          3306 is MySQL default.
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          DB User:
	        </td>
	        <td align="left">
	          <input name="db_user" type="text" id="db_user" value="root" size="24" maxlength="50">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          DB Pass:
	        </td>
	        <td align="left">
	          <input name="db_password" type="text" id="db_password" size="24" maxlength="64">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          Database:
	        </td>
	        <td align="left">
	          <input name="db_name" type="text" id="db_name" size="24" maxlength="64">
	        </td>
	      </tr>
	    </table>
	        <p>&nbsp;</p>

	    <table width="400" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td>&nbsp;
	        </td>
	        <td align="left">
	          <strong>Other Config Settings</strong></td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          Master E-mail:
	        </td>
	        <td align="left">
	          <input name="admin_email" type="text" id="admin_email" size="30" maxlength="50">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          First Timeout:
	        </td>
	        <td align="left">
	          <input name="global_timeout" type="text" id="global_timeout" value="10" size="5" maxlength="3">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          Second Timeout:
	        </td>
	        <td align="left">
	          <input name="global_timeout_secondary" type="text" id="global_timeout_secondary" value="5" size="5" maxlength="3">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          Absolute Path:
	        </td>
	        <td align="left">
	          <input name="abs_path" type="text" id="abs_path" value="$abspath" size="40" maxlength="250">
	        </td>
	      </tr>
	      <tr>
	        <td width="120" align="right">
	          Relative Path:
	        </td>
	        <td align="left">
	          <input name="relative_path" type="text" id="relative_path" value="$relpath" size="40" maxlength="250">
	        </td>
	      </tr>
	    </table>
	        <p><br>
	          <input type="submit" name="InstallNow" value="Install netMon">
	        </p>
	      </td>
	    </tr>
	  </form>
	</table>
	</body>
	</html>
INSTALL_FORM;
}

if (isset($site_abspath) && isset($site_url))
  require ($site_abspath . "/footer.tpl");

?>
