<?php

##############################################################################################################################
## Following is our custom error handler, so we don't have to rely on the php.ini settings and can make our errors prettier.
##
function netmonErrorHandler($errno, $errmsg, $filename, $linenum, $vars)
{
	//global $errhdlr_screen, $errhdlr_log, $errhdlr_mail;

	// timestamp for the error entry
	date_default_timezone_set('America/New_York');
	$dt = date("Y-m-d H:i:s (T)");

	// define an assoc array of error string, recommend the only entries we should
	// consider are E_WARNING, E_NOTICE, E_USER_ERROR, E_USER_WARNING and E_USER_NOTICE
	$errortype = array (
								E_ERROR           => "Error",
								E_WARNING         => "Warning",
								E_PARSE           => "Parsing Error",
								E_NOTICE          => "Notice",
								E_CORE_ERROR      => "Core Error",
								E_CORE_WARNING    => "Core Warning",
								E_COMPILE_ERROR   => "Compile Error",
								E_COMPILE_WARNING => "Compile Warning",
								E_USER_ERROR      => "User Error",
								E_USER_WARNING    => "User Warning",
								E_USER_NOTICE     => "User Notice",
								E_STRICT          => "Runtime Notice",
								E_DEPRECATED      => "Depreciated",
								E_USER_DEPRECATED => "User Depreciated",
								E_ALL             => "Other Fatal"
						 );
	// set of errors for which a var trace will be saved
	//$user_errors = array(E_ERROR, E_WARNING, E_USER_ERROR, E_USER_WARNING);
	//$user_errors = array(E_ALL);

	$err = "<span style=\"font: 12px Tahoma, Arial, sans-serif;\"><b>netMon Script Issue:</b> An error has been generated in one of your scripts at $dt \n";
	$err .= "<b>$errortype[$errno] ($errno)</b> $errmsg <b>$filename $linenum</b></span>\n\n";

	// save to the error log, and e-mail me if there is a critical user error
	//error_log($err, 3, "include/err_log");
	//if ($errno != E_USER_ERROR || E_USER_WARNING || E_USER_NOTICE) {
		//if ($errhdlr_screen == TRUE)
			echo nl2br($err);

/*
		if ($errhdlr_log == TRUE)
			error_log($err, 3, "include/err_log");
		if ($errhdlr_mail == TRUE)
			mail($ticketad, "netMon Internal Error", $err); */
	 //}
}


##############################################
## Function notify_msg
##
function notify_msg($message, $msgtype='notify', $return = false)
{
	global $notifyMsg;

  if ($msgtype == 'success')
    $msgOut = "<div class=\"successmsg\">Success: $message</div>";
  elseif ($msgtype == 'notify')
    $msgOut = "<div class=\"notifymsg\">Notice: $message</div>";
  elseif ($msgtype == 'error')
    $msgOut = "<div class=\"errormsg\">Error: $message</div>";
  else
    $msgOut = "<div class=\"errormsg\">An incorrect error message has been shown, please notify the webmaster via the contact page.</div>";

 	if (!$return) {
	  echo $msgOut;
  }
	else {
		//$notifyMsg .= $msgOut;
		return $msgOut;
	}
}


######################################################################################################
## Function notify_msg_fullpage
## wraps the above notify_msg function with a full page header/footer (for proper html page display)
##
function notify_msg_fullpage($message, $msgtype='notify')
{
	global $site_abspath, $site_url, $include_url, $thisPage, $navbar, $userinfo, $notifyMsg, $jQueryFunctions;

	require_once ($site_abspath . '/header.tpl');
	notify_msg($message, $msgtype);
	require_once ($site_abspath . '/footer.tpl');
}


##############################################
## Function authenticate_user
##
function authenticate_user($uname, $passwd) {

  global $db;

  //$passwd = hash('sha256', $passwd);
  $confirm = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_USERS . " WHERE username='" . $uname . "' AND password='" . $passwd . "';");

  if ($confirm) {
    $userinfo = $db->get_row('SELECT * FROM ' . DB_TABLE_USERS . " WHERE username='" . $uname . "' AND password='" . $passwd . "';");
    $userinfo->usergroups = $db->get_results('SELECT groupid FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='" . $userinfo->userid . "';");
    return $userinfo;
  }
  else {
		$userinfo = new stdClass;
		$userinfo->userid = 0;
    return $userinfo;
  }
}


/***********************************
* Function display_login
***********************************/

function display_login() {

	global $thisPage;

print <<<LOGIN
    &nbsp;
    <div style="width: 500px; background-color: #f0f0f0; border: 2px solid #444444; margin-left: auto; margin-right: auto; text-align: center;">
      &nbsp;
      <form name="login" method="post" action="$thisPage">
            Please Login<br>
            <br>
            <table width="200" cellspacing="2" cellpadding="0" border="0" style="margin-left: auto; margin-right: auto;">
              <tr>
                <td align="right">Username: </td>
                <td align="left"> <input name="username_login" type="text" size="24" maxlength="30"></td>
              </tr>
              <tr>
                <td align="right">Password: </td>
                <td align="left"> <input name="password_login" type="password" size="24" maxlength="64"></td>
              </tr>
            </table>
            <br> <input type="submit" name="Submit" value="Login"> <br>
      </form>
      &nbsp;
    </div>
LOGIN;

}


####################################
## Function user_add
##
function user_add($username, $passwd, $passwd_confirm, $email, $email_notify = 1, $admin_lvl = '10', $firstname = '', $lastname = '', $aim = '', $aim_notify = 0, $phone = '###-###-####', $sms = '###-###-####', $sms_notify = 0) {

  global $db;

  if ($username != NULL && $passwd != NULL && $passwd_confirm != NULL && $email != NULL)
  {
    $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_USERS . " WHERE username='$username' OR email='$email'");
    if (!$exists) {
      $success = strcmp($passwd, $passwd_confirm);

      if ($success == 0) {
        $passwd = hash('sha256', $passwd);

        /* create user */
        $sql = 'INSERT INTO ' . DB_TABLE_USERS . ' (username, password, email, email_notify, admin_lvl, name_first, name_last, aim, aim_notify, phone, sms, sms_notify) VALUES ' . "('$username', '$passwd', '$email', '$email_notify', '$admin_lvl', '$firstname', '$lastname', '$aim', '$aim_notify', '$phone', '$sms', '$sms_notify')";
        $success = $db->query($sql);
        if ($success == true) {
          notify_msg('User `' . $username . '` successfully created.');
        }
      }
      else {
        print 'Passwords do not match: <br>' . $passwd . '<br>' . $passwd_confirm;
        $success = 0;
      }
    }
    else {  // if $exists has a value other than 0
      notify_msg('It seems this username or email already exists, please enter a more unique username.', 'error');
    }
  }
  else {
    notify_msg('Not all the required information to create a new user was entered.', 'error');
    $success = 0;
  }

  return $success;
}


####################################
## Function user_update
##
function user_update($userid, $username, $passwd = NULL, $passwd_confirm = NULL, $email = '', $email_notify = 0, $firstname = '', $lastname = '', $admin_lvl, $aim = '', $aim_notify = 0, $phone = '###-###-####', $sms = '###-###-####', $sms_notify = 0) {

  global $db, $userinfo;

  $exists = $db->get_var('SELECT userid FROM ' . DB_TABLE_USERS . " WHERE username='$username' AND userid<>'$userid' OR email='$email' AND userid<>'$userid'");
  if (!$exists || $exists->userid == $userid) {
    if ($passwd != NULL) {
      $pwd_success = strcmp($passwd, $passwd_confirm);

      if ($pwd_success == 0) {    // if password value was NOT passed
        $passwd = hash('sha256', $passwd);

        $sql = 'UPDATE ' . DB_TABLE_USERS . " SET username='$username', password='$passwd', email='$email', email_notify='$email_notify', name_first='$firstname', name_last='$lastname', admin_lvl='$admin_lvl', aim='$aim', aim_notify='$aim_notify', phone='$phone', sms='$sms', sms_notify='$sms_notify' WHERE userid='$userid'";
        $success = $db->query($sql);
        if ($success == true) {
          notify_msg('User `' . $username . '` info and password successfully updated.');
        }
      }
      else {
        notify_msg('Passwords do not match,<br>passwd length: ' . strlen($passwd) . '<br>verify length: ' . strlen($passwd_confirm) . '<br><br>User info not updated, try again.', 'error');
        $success = 0;
      }
    }
    else {    // if password value was passed
      $sql = 'UPDATE ' . DB_TABLE_USERS . " SET username='$username', email='$email', email_notify='$email_notify', name_first='$firstname', name_last='$lastname', admin_lvl='$admin_lvl', aim='$aim', aim_notify='$aim_notify', phone='$phone', sms='$sms', sms_notify='$sms_notify' WHERE userid='$userid'";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('User `' . $username . '` info successfully updated.');
      }
    }
  }
  else {
    notify_msg('Either the username or the e-mail address you have selected already exist, both must be unique from all other users. Please hit the reload button (upper left) to continue editing this user.', 'error');
    $success = 0;
  }

  return $success;
}


/***********************************
* Function user_delete
***********************************/

function user_delete($user_id) {

  global $db;
  global $userinfo;

  $group_match = 0;
  $deleteusergroups = $db->get_results('SELECT groupid FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='$user_id'");

  if (isset($userinfo->usergroups)) {
    foreach ($userinfo->usergroups as $usergroup) {
      foreach($deleteusergroups as $deleteusergroup) {
        if ($usergroup->groupid == $deleteusergroup->groupid)
          $group_match = 1;
      }
    }
  }

  if ($userinfo->userid == $user_id && $userinfo->admin_lvl < ADMINLVL_MASTER)
  {
    notify_msg('You can not delete yourself unless you are a master admin.', 'error');
  }
  else{
    if ($group_match || $userinfo->admin_lvl >= ADMINLVL_MASTER) {
      /* code to remove user from all groups it is assigned to */
      $sql = 'DELETE FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='$user_id'";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('User successfully removed from all groups it was assigned to.');
      }

      /* code to remove actual userr */
      $sql = 'DELETE FROM ' . DB_TABLE_USERS . " WHERE userid='$user_id'";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('User fully removed from system completely.');
      }
    }
  }

}


/***********************************
* Function user_add_to_group
***********************************/

function user_add_to_group($user_id, $group_id) {

  global $db;

  if ($user_id != NULL && $group_id != NULL) {
    $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_USER_GROUP . " WHERE groupid='$group_id' AND userid='$user_id'");
    if (!$exists) {
      /* remove from unassigned group (group 0) */
      $sql = 'DELETE FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='$user_id' AND groupid='0'";
      $success = $db->query($sql);

      /* assign to new group */
      $sql = 'INSERT INTO ' . DB_TABLE_USER_GROUP . ' (groupid, userid) VALUES ' . "('$group_id', '$user_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('User successfully added to group.');
      }
    }
    else {
        notify_msg('User is already assigned to this group.', 'error');
    }
  }
  else {
    notify_msg('A group was not selected. User is being assigned to group `0` - which is unassigned.');
    $sql = 'INSERT INTO ' . DB_TABLE_USER_GROUP . ' (groupid, userid) VALUES ' . "('0', '$db->insert_id')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('User successfully assigned to group `0`.');
    }
  }
}


/***********************************
* Function user_remove_from_group
***********************************/

function user_remove_from_group($user_id, $group_id) {

  global $db;
  global $userinfo;

  if ($userinfo->userid == $user_id && $userinfo->admin_lvl < ADMINLVL_MASTER)
  {
    notify_msg('You can not remove yourself from a group unless you are a master admin.', 'error');
  }
  else
  {

    /* delete user from specified group */
    $sql = 'DELETE FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='$user_id' AND groupid='$group_id'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('User removed from old group successfully.');
    }

    /* code to assign user to group 0 (unassigned) only if user is not assigned to another group */
    $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_USER_GROUP . " WHERE userid='$user_id'");
    if (!$exists) {
      $sql = 'INSERT INTO ' . DB_TABLE_USER_GROUP . " (userid, groupid) VALUES ('$user_id', '0')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('User assigned to group 0 (unassigned).');
      }
    }

  }

}


/***********************************
* Function group_add
***********************************/

function group_add($group_name) {

  global $db;
  $mantis_link = '[<a href="http://mantis.trinitywebs.com" target="_blank">TW Mantis Bug Tracker</a>]';

  $sql = 'INSERT INTO ' . DB_TABLE_GROUPS . ' (groupname) VALUES ' . "('$group_name')";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Group `' . $group_name . '` successfully created.');
  }
  else {
    notify_msg('There seems to have been an error creating `' . $group_name . '`. If this persists please visit the bug reporting site located here ' . $mantis_link . ' and report the bug with as much details as possible so we can attempt to recreate the situation', 'error');
  }

}


/***********************************
* Function group_update
***********************************/

function group_update($curr_group_id, $new_group_name) {

  global $db;

  $sql = 'SELECT groupname FROM ' . DB_TABLE_GROUPS . " WHERE groupid='$curr_group_id'";
  $curr_group_name = $db->get_var($sql);

  if ($curr_group_id != NULL && $new_group_name != NULL)
  {
    $sql = 'UPDATE ' . DB_TABLE_GROUPS . " SET groupname='$new_group_name' WHERE groupid='$curr_group_id'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port group title `' . $curr_group_name . '` successfully changed to `' . $new_group_name . '`.');
    }
  }
  else
  {
    notify_msg('Either a group was not selected or a new name was not entered.', 'error');
  }

}


####################################
## Function group_delete
##
function group_delete($group_id) {

  global $db;

  $sql = 'SELECT groupname FROM ' . DB_TABLE_GROUPS . " WHERE groupid='$group_id'";
  $curr_group_name = $db->get_var($sql);

  $sql = 'UPDATE ' . DB_TABLE_USER_GROUP . " SET groupid='0' WHERE groupid='$group_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Unassigning users from `' . $curr_group_name . '`. Success.');
  }

  $sql = 'UPDATE ' . DB_TABLE_SERVER_GROUP . " SET groupid='0' WHERE groupid='$group_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Unassigning servers from `' . $curr_group_name . '`. Success.');
  }

  $sql = 'DELETE FROM ' . DB_TABLE_GROUPS . " WHERE groupid='$group_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Group `' . $curr_group_name . '` successfully deleted.');
  }

  /* go back though the list of all the servers and make sure they are not assigned to more than just group 0, if so, delete group 0 assignment */
  $servers = $db->get_results("SELECT serverid FROM " . DB_TABLE_SERVERS);
  if ($servers) {
    foreach ( $servers as $server )
    {
      $group_count = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server->serverid'");
      if ($group_count > 1) {
          $db->query('DELETE FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server->serverid' AND groupid='0'");
      }
    }
  }

  /* the server check that is done in the few lines above will need to be done for users as well when I get to that point (may want to omit master user though) */

}


/***********************************
* Function server_add
***********************************/
function server_add($host_name, $group_id = NULL, $server_name = NULL, $company = NULL) {

  global $db;

  $ip_address = gethostbyname($host_name . '.');
/* DEBUG
		echo '<p>hn: ' . $host_name;
		echo '<br/ >ip: ' . $ip_address . '</p>\'';
		print_r($host_name);
		echo '\'|\'';
		print_r($ip_address);
		echo '\'';
 */
	if (!strcmp($host_name . '.', $ip_address)) {   // !strcmp means if they ARE equal
    notify_msg('Server `' . $host_name . '` can not resolve to an IP address.<br>Server will not be added if the hostname does not resolve.<br>You are not allowed to add only IP addresses directly at this time.', 'error');
  }
  else {
    $sql = 'INSERT INTO ' . DB_TABLE_SERVERS . ' (hostname, ip_address, servername, company) VALUES ' . "('$host_name', '$ip_address', '$server_name', '$company')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Server `' . $host_name . '` successfully created/added.');
    }
    if ($group_id != NULL) {
      $sql = 'INSERT INTO ' . DB_TABLE_SERVER_GROUP . ' (groupid, serverid) VALUES ' . "('$group_id', '$db->insert_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Server `' . $host_name . '` successfully assigned to group.');
      }
    }
    else {
      notify_msg('Notice: A group was not selected. Server is being assigned to group `0` - which is unassigned.');
      $sql = 'INSERT INTO ' . DB_TABLE_SERVER_GROUP . ' (groupid, serverid) VALUES ' . "('0', '$db->insert_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Server `' . $host_name . '` successfully assigned to group `0`.');
      }
    }
  }
}


####################################
## Function server_update
##
function server_update($server_id, $group_id = NULL, $host_name = NULL, $server_name = NULL, $company = NULL, $server_info = NULL, $maint_mode = 0) {

  global $db;

  //$ip_address = gethostbyname($host_name);
  //if (!strcmp($host_name, $ip_address)) {
    //notify_msg('Server `' . $host_name . '` can not resolve to an IP address. At this time, this software will not allow you to update a server if the hostname does not resolve. This will be fixed in the future on request.');
  //}
  if ($host_name == NULL && $server_name == NULL && $company == NULL && $server_id != NULL && $group_id != NULL) {
    //$sql = 'UPDATE ' . DB_TABLE_SERVERS . ' SET hostname='$host_name', ip_address='$ip_address', servername='$server_name', company='$company' WHERE serverid='$server_id'";
    $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_GROUP . " WHERE groupid='$group_id' AND serverid='$server_id'");
    if (!$exists) {
      /* remove from unassigned group (group 0) */
      $sql = 'DELETE FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server_id' AND groupid='0'";
      $success = $db->query($sql);

      /* assign to new group */
      $sql = 'INSERT INTO ' . DB_TABLE_SERVER_GROUP . ' (groupid, serverid) VALUES ' . "('$group_id', '$server_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Server `' . $server_id . '` successfully updated.');
      }
    }
    else {
        notify_msg('Server `' . $server_id . '` is already assigned to this group.', 'error');
    }
    /*if ($group_id != NULL) {
      $sql = 'INSERT INTO ' . DB_TABLE_SERVER_GROUP . ' (groupid, serverid) VALUES ' . "('$group_id', '$server_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Server `' . $host_name . '` successfully assigned to group.');
      }
    }
    else {
      notify_msg('Notice: Server was not added to a group. You may wish to assign it to one.');
    }*/
  }
  elseif ($host_name != NULL && $group_id == NULL) {
    //echo '<b>TEST 2!</b>';
    $ip_address = gethostbyname($host_name);
    $sql = 'UPDATE ' . DB_TABLE_SERVERS . " SET servername='$server_name', hostname='$host_name', ip_address='$ip_address', company='$company', server_info='$server_info', maint_mode='$maint_mode' WHERE serverid='$server_id'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Server `' . $host_name . '` info successfully updated.');
    }
  }
}


/***********************************
* Function server_delete
***********************************/

function server_delete($server_id) {

  global $db;

  /* code to remove server from all groups it is assigned to */
  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Server successfully removed from all groups it was assigned to.');
  }

  /* code to remove server from all groups it is assigned to */
  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_INDV_PORT . " WHERE serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('All ports assigned to server have been removed.');
  }

  /* code to remove server from all groups it is assigned to */
  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('All ports groups assigned to server have been removed.');
  }

  /* code to remove server from all groups it is assigned to */
  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PORT . " WHERE serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Combined/full port list assigned to server has been removed.');
  }

  /* code to remove server from all groups it is assigned to */
  $sql = 'DELETE FROM ' . DB_TABLE_STATUS_LOG . " WHERE serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('All port entries in status log for this server have been removed.');
  }

  /* code to remove actual server */
  $sql = 'DELETE FROM ' . DB_TABLE_SERVERS . " WHERE serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Server completely removed from system.');
  }

}


/***********************************
* Function server_remove_from_group
***********************************/

function server_remove_from_group($server_id, $group_id) {

  global $db;

  /* delete server from specified group */
  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server_id' AND groupid='$group_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Server removed from old group successfully.');
  }

  /* code to assign server to group 0 (unassigned) only if server is not assigned to another group*/
  $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server_id'");
  if (!$exists) {
    $sql = 'INSERT INTO ' . DB_TABLE_SERVER_GROUP . " (serverid, groupid) VALUES ('$server_id', '0')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Server assigned to group 0 (unassigned).');
    }
  }
}


######################################
## Function server_assign_pgroup
## Assigns a port group to a server
##
function server_assign_pgroup($server_id, $pgroup_id) {

  global $db;
  //$groupsexist = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE serverid='$server_id'");
  $portlist_before = get_full_port_list($server_id, TRUE);
  if (!$portlist_before) {
    $portlist_before = array();
  }

  /* code to assign server to group 0 (unassigned) only if server is not assigned to another group*/
  $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE serverid='$server_id' AND pgroupid='$pgroup_id'");
  if (!$exists) {
    $sql = 'INSERT INTO ' . DB_TABLE_SERVER_PGROUP . " (pgroupid, serverid) VALUES ('$pgroup_id', '$server_id')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port group assigned to server.');
    }
    $portlist_after = get_full_port_list($server_id, TRUE);
    $portlist_modified = array_diff($portlist_after, $portlist_before);
    if (ADMIN_DEBUG) {
      print_r($portlist_after);
      echo '<br><br>';
      print_r($portlist_before);
      echo '<br><br>';
      print_r($portlist_modified);
    }
    if ($portlist_modified) {
      foreach ( $portlist_modified as $currentportid )
      {
        $sql = 'INSERT INTO ' . DB_TABLE_SERVER_PORT . " (serverid, portid, current_status) VALUES ('$server_id', '$currentportid', '0')";
        $success = $db->query($sql);
      }
    }
  }
  else {
    notify_msg('This port group is already assigned to this server.', 'error');
  }

}


/***********************************
* Function server_remove_pgroup
***********************************/

function server_remove_pgroup($server_id, $pgroup_id, $update_logs = true) {

  global $db;
  $timestamp = time();  // used to update time ports were removed
  $portlist_before = get_full_port_list($server_id, TRUE);

  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE pgroupid='$pgroup_id' AND serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Port group removed.');
  }
  $portlist_after = get_full_port_list($server_id, TRUE);
  if (!$portlist_after) {
    $portlist_after = array();
  }
	if (empty($portlist_before))
		$portlist_before = array();
	if (empty($portlist_after))
		$portlist_after = array();
  $portlist_modified = array_diff($portlist_before, $portlist_after);
  if (ADMIN_DEBUG) {
    print_r($portlist_after);
    echo '<br><br>';
    print_r($portlist_before);
    echo '<br><br>';
    print_r($portlist_modified);
  }
  if ($portlist_modified) {
    foreach ( $portlist_modified as $currentportid )
    {
      $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PORT . " WHERE serverid='$server_id' AND portid='$currentportid'";
      $success = $db->query($sql);
      if ($update_logs)
        log_entry_port($server_id, $currentportid, $timestamp, '4', '0', 'Port removed from server', NULL);
    }
  }

}


/***********************************
* Function server_assign_indv_port
***********************************/

function server_assign_indv_port($server_id, $port_id) {

  global $db;
  //$groupsexist = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_PORT . " WHERE serverid='$server_id'");
  $portlist_before = get_full_port_list($server_id, TRUE);
  if (!$portlist_before) {
    $portlist_before = array();
  }

  /* code to assign server to group 0 (unassigned) only if server is not assigned to another group*/
  $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_SERVER_INDV_PORT . " WHERE serverid='$server_id' AND portid='$port_id'");
  if (!$exists) {
    $sql = 'INSERT INTO ' . DB_TABLE_SERVER_INDV_PORT . " (portid, serverid) VALUES ('$port_id', '$server_id')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Individual port assigned to server.');
    }
    $portlist_after = get_full_port_list($server_id, TRUE);
    $portlist_modified = array_diff($portlist_after, $portlist_before);
    if (ADMIN_DEBUG) {
      print_r($portlist_after);
      echo '<br><br>';
      print_r($portlist_before);
      echo '<br><br>';
      print_r($portlist_modified);
    }
    if ($portlist_modified) {
      foreach ( $portlist_modified as $currentportid )
      {
        $sql = 'INSERT INTO ' . DB_TABLE_SERVER_PORT . " (serverid, portid, current_status) VALUES ('$server_id', '$currentportid', '0')";
        $success = $db->query($sql);
      }
    }
  }
  else {
    notify_msg('This individual port is already assigned to this server.', 'error');
  }

}


/***********************************
* Function server_remove_indv_port
***********************************/

function server_remove_indv_port($server_id, $port_id) {

  global $db;
  $timestamp = mktime();  // used to update time port was removed
  $portlist_before = get_full_port_list($server_id, TRUE);

  $sql = 'DELETE FROM ' . DB_TABLE_SERVER_INDV_PORT . " WHERE portid='$port_id' AND serverid='$server_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Port removed.');
  }
  $portlist_after = get_full_port_list($server_id, TRUE);
  if (!$portlist_after) {
    $portlist_after = array();
  }
  $portlist_modified = array_diff($portlist_before, $portlist_after);
  if (ADMIN_DEBUG) {
    print_r($portlist_after);
    echo '<br><br>';
    print_r($portlist_before);
    echo '<br><br>';
    print_r($portlist_modified);
  }
  if ($portlist_modified) {
    foreach ( $portlist_modified as $currentportid )
    {
      $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PORT . " WHERE serverid='$server_id' AND portid='$currentportid'";
      $success = $db->query($sql);
      log_entry_port($server_id, $currentportid, $timestamp, '4', '0', 'Port removed from server', NULL);
    }
  }

}


/***********************************
* Function pgroup_add
***********************************/

function pgroup_add($pgroup_name) {

  global $db;
  $mantis_link = '[<a href="http://mantis.trinitywebs.com" target="_blank">TW Mantis Bug Tracker</a>]';

  if ($_POST['pgroup'] != NULL)
  {
    $sql = 'INSERT INTO ' . DB_TABLE_PGROUPS . ' (pgroupname) VALUES ' . "('$pgroup_name')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port group `' . $pgroup_name . '` successfully created.');
    }
    else {
      notify_msg('There seems to have been an error creating `' . $pgroup_name . '`. If this persists please visit the bug reporting site located here ' . $mantis_link . ' and report the bug with as much details as possible so we can attempt to recreate the situation', 'error');
    }
  }
  else
  {
    notify_msg('Error: You need to specify a group name.', 'error');
  }


}


/***********************************
* Function pgroup_update
***********************************/

function pgroup_update($curr_pgroup_id, $new_pgroup_name) {

  global $db;

  /* next two lines simply get the existing group name */
  $sql = 'SELECT pgroupname FROM ' . DB_TABLE_PGROUPS . " WHERE pgroupid='$curr_pgroup_id'";
  $curr_pgroup_name = $db->get_var($sql);

  if ($curr_pgroup_id != NULL && $new_pgroup_name != NULL)
  {
    $sql = 'UPDATE ' . DB_TABLE_PGROUPS . " SET pgroupname='$new_pgroup_name' WHERE pgroupid='$curr_pgroup_id'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port group title `' . $curr_pgroup_name . '` successfully changed to `' . $new_pgroup_name . '`.');
    }
  }
  else
  {
    notify_msg('Either a port group was not selected or a new name was not entered.', 'error');
  }
}


/***********************************
* Function pgroup_delete
***********************************/

function pgroup_delete($pgroup_id) {

  global $db;

  /* delete server from specified group */

  /* code to assign user to group 0 (unassigned) only if user is not assigned to another group*/
  $sql = 'UPDATE ' . DB_TABLE_PORT_GROUP . " SET pgroupid='0' WHERE pgroupid='$pgroup_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Ports assigned to group 0 (unassigned).');
    $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE pgroupid='$pgroup_id'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Group removed from all existing servers.');
      $sql = 'DELETE FROM ' . DB_TABLE_PGROUPS . " WHERE pgroupid='$pgroup_id'";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Group removed from old group successfully.');
      }
    }
  }

  /* go back though the list of all the ports and make sure they are not assigned to more than just group 0, if so, delete group 0 assignment */
  $ports = $db->get_results("SELECT portid FROM " . DB_TABLE_PORT_GROUP);
  if ($ports) {
    foreach ( $ports as $port )
    {
      $group_count = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_PORT_GROUP . " WHERE portid='$port->portid'");
      if ($group_count > 1) {
          $db->query('DELETE FROM ' . DB_TABLE_PORT_GROUP . " WHERE portid='$port->portid' AND pgroupid='0'");
      }
    }
  }

}


####################################
## Function port_add
##
function port_add($port_name, $port, $pgroup_id = NULL) {

  if ($port_name !=NULL && $port != NULL)
  {
    global $db;

    $sql = 'INSERT INTO ' . DB_TABLE_PORTS . ' (portname, port) VALUES ' . "('$port_name', '$port')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port `' . $port_name . " ($port)" . '` successfully created/added.');
    }

    /* if group is set, assign to it, if not, assign to group 0 */
    if ($pgroup_id != NULL) {
      $sql = 'INSERT INTO ' . DB_TABLE_PORT_GROUP . ' (pgroupid, portid) VALUES ' . "('$pgroup_id', '$db->insert_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Server `' . $port_name . '` successfully assigned to port group.');
      }
    }
    else {
      notify_msg('A group was not selected. Server is being assigned to port group `0`, which is Unassigned.');
      $sql = 'INSERT INTO ' . DB_TABLE_PORT_GROUP . ' (pgroupid, portid) VALUES ' . "('0', '$db->insert_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Server `' . $port_name . '` successfully assigned to port group `0`, which is Unassigned.');
      }
    }
  }
  else
  {
    notify_msg('Either a port name or an actual port number was not entered.', 'error');
  }
}


####################################
## Function port_delete
##
function port_delete($port_id) {

    global $db;

    $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PORT . " WHERE portid='" . $port_id . "'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port ID `' . $port_id . '` successfully removed from current server port assignments.');
    }

    $sql = 'DELETE FROM ' . DB_TABLE_SERVER_INDV_PORT . " WHERE portid='" . $port_id . "'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port ID `' . $port_id . '` successfully removed from server individual port assignments.');
    }

    $sql = 'DELETE FROM ' . DB_TABLE_PORT_GROUP . " WHERE portid='" . $port_id . "'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port ID `' . $port_id . '` successfully removed from port groups.');
    }

    $sql = 'DELETE FROM ' . DB_TABLE_STATUS_LOG . " WHERE portid='" . $port_id . "'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port ID `' . $port_id . '` successfully removed from status log.');
    }

    $sql = 'DELETE FROM ' . DB_TABLE_PORTS . " WHERE portid='" . $port_id . "'";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port ID `' . $port_id . '` successfully deleted.');
    }

}


####################################
## Function port_add_to_group
##
function port_add_to_group($port_id, $pgroup_id) {

  global $db;

  if ($port_id != NULL && $pgroup_id != NULL) {
    $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_PORT_GROUP . " WHERE pgroupid='$pgroup_id' AND portid='$port_id'");
    if (!$exists) {
      /* remove from unassigned group (group 0) */
      $sql = 'DELETE FROM ' . DB_TABLE_PORT_GROUP . " WHERE portid='$port_id' AND pgroupid='0'";
      $success = $db->query($sql);

      /* assign to new group */
      $sql = 'INSERT INTO ' . DB_TABLE_PORT_GROUP . ' (pgroupid, portid) VALUES ' . "('$pgroup_id', '$port_id')";
      $success = $db->query($sql);
      if ($success == true) {
        notify_msg('Port successfully added to group.');
      }

      /* this will add the port to the servers that are already assigned to this group */
      $existing_servers = $db->get_results('SELECT serverid FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE pgroupid='$pgroup_id'");
      if ($existing_servers) {
        foreach ( $existing_servers as $existing_server ) {
          $sql = 'INSERT INTO ' . DB_TABLE_SERVER_PORT . " (serverid, portid, current_status) VALUES ('$existing_server->serverid', '$port_id', '0')";
          $success = $db->query($sql);
        }
      }

    }
    else {
        notify_msg('Port is already assigned to this group.', 'error');
    }
  }
  else {
    notify_msg('Notice: A port group was not selected. User is being assigned to port group `0` - which is unassigned.');
    $sql = 'INSERT INTO ' . DB_TABLE_PORT_GROUP . ' (pgroupid, portid) VALUES ' . "('0', '$db->insert_id')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('User successfully assigned to port group `0`.');
    }
  }

}


####################################
## Function port_remove_from_group
##
function port_remove_from_group($port_id, $pgroup_id) {

  global $db;

  /* this will remove the port from the servers that are already assigned to this group */
  $existing_servers = $db->get_results('SELECT serverid FROM ' . DB_TABLE_SERVER_PGROUP . " WHERE pgroupid='$pgroup_id'");
  if ($existing_servers) {
    foreach ( $existing_servers as $existing_server ) {
      $sql = 'DELETE FROM ' . DB_TABLE_SERVER_PORT . " WHERE serverid='$existing_server->serverid' AND portid='$port_id'";
      $success = $db->query($sql);
    }
  }

  /* delete from specified group */
  $sql = 'DELETE FROM ' . DB_TABLE_PORT_GROUP . " WHERE portid='$port_id' AND pgroupid='$pgroup_id'";
  $success = $db->query($sql);
  if ($success == true) {
    notify_msg('Port removed from old port group successfully.');
  }

  /* code to assign to group 0 (unassigned) only if user is not assigned to another group*/
  $exists = $db->get_var('SELECT count(*) FROM ' . DB_TABLE_PORT_GROUP . " WHERE portid='$port_id'");
  if (!$exists) {
    $sql = 'INSERT INTO ' . DB_TABLE_PORT_GROUP . " (portid, pgroupid) VALUES ('$port_id', '0')";
    $success = $db->query($sql);
    if ($success == true) {
      notify_msg('Port assigned to port group 0 (unassigned).');
    }
  }
}


######################################################################
## Function display_server_summary_grid
## Immediately displays summary grid when called, no return values
##
function display_server_summary_grid($pgroup_id = FALSE, $server_id = FALSE) {

  global $db, $userinfo, $images_url;

  // get the list of servers
  if ($userinfo->admin_lvl < ADMINLVL_MASTER) {
    //echo 'exec this 3<br>';
    //echo $userinfo->admin_lvl;
    $sql = 'SELECT s.serverid, s.hostname, s.current_status
      FROM ' . DB_TABLE_USER_GROUP . " ug, " . DB_TABLE_SERVER_GROUP . " sg, " . DB_TABLE_SERVERS . " s
      WHERE ug.userid = '$userinfo->userid'
        AND sg.groupid = ug.groupid
        AND s.serverid = sg.serverid
        ORDER BY sg.groupid, s.serverid";
    $servers = $db->get_results($sql);
  }
  else if ($userinfo->admin_lvl >= ADMINLVL_MASTER) {
    if ($pgroup_id) {
      //echo 'exec this';
      $sql = 'SELECT s.serverid, s.hostname, s.current_status
        FROM ' . DB_TABLE_SERVER_GROUP . " sg, " . DB_TABLE_SERVERS . " s
        WHERE sg.groupid = '$pgroup_id'
          AND s.serverid = sg.serverid
          ORDER BY sg.groupid, s.serverid";
      $servers = $db->get_results($sql);
    }
    else {
      //echo 'exec this 2';
      $servers = $db->get_results('SELECT serverid, hostname, current_status FROM ' . DB_TABLE_SERVERS . ' ORDER BY serverid');
    }
  }
  //$servers = $db->get_results("SELECT serverid, hostname, current_status FROM " . DB_TABLE_SERVERS);

  if ($servers) {
    $server_port_lists = '';
    $i = 0;
    $add_comma = '';
    foreach ( $servers as $server ) {
      //$server->ports = $db->get_results("SELECT portid, current_status FROM " . DB_TABLE_SERVER_PORT . " WHERE serverid='$server->serverid'");
      $server_port_list[$i] = get_full_port_list($server->serverid, TRUE);
      //print_r($server);
      //print_r($server_port_list);
      //echo '<br><br>';
      if (isset($server_port_list[$i]) && !empty($server_port_list[$i])) {
        $server_port_lists .= $add_comma . '$server_port_list[' . $i . ']';
        $i++;
        $add_comma = ', ';
      }
    }
    $port_list = $db->get_col("SELECT portid FROM " . DB_TABLE_PORTS . ' ORDER BY port');
		//if (isset($port_list) && !empty($port_list))
			//$port_list = array();
		$exported = var_export($port_list, true);
    echo "<pre>Port List (All):\n". $exported .'</pre>';
		//echo $server_port_lists;
		$exported = var_export($server_port_list, true);
    echo "<pre>Port List (Server):\n". $exported .'</pre>';

		// Following if statement protects the script from errors when no ports are assigned to any servers (will only be the case after initial install).
    if (isset($server_port_list) && !empty($server_port_list))
			$intersecting_ports = array_diff($port_list, $server_port_list);
      //eval($eval_intersection);
    if (isset($intersecting_ports) && is_array($intersecting_ports))
      $intersecting_ports = array_diff($port_list, $intersecting_ports);
    //echo '<br><br>';
    print_r($intersecting_ports);
  }

  if (isset($intersecting_ports)) {
    $grid_num_cols = count($intersecting_ports) + 4;
    echo '<table width="700" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#444444">
      <tr bgcolor="ffffff"><td align="center" colspan="' . $grid_num_cols . '" class="table-heading"><b>&nbsp;&nbsp;&nbsp;&nbsp;Server Status Grid Overview</b></td></tr>
      <tr bgcolor="ffffff"><td align="center"><b>ID</b></td>
      <td align="center"><b>Overall</b></td>
      <td align="center"><b>Up</b></td>
      <td align="center"><b>Hostname<br><img src="' . $images_url . '/shim.gif" height="5"
width="200"></b></td>
      ';

    $j = 0;
    foreach ( $intersecting_ports as $grid_port_id )
    {
      $sql = "SELECT * FROM " . DB_TABLE_PORTS . " WHERE portid=$grid_port_id";
      $grid_port_data = $db->get_row($sql);
      //print_r($grid_port_data);
      $grid_port_name = $grid_port_data->portname;
      $grid_port_value = $grid_port_data->port;
      echo '<td align="center"><b>' . $grid_port_name . '<br>(' . $grid_port_value . ')</b></td>' . "\n";
      $j++;
    }
    echo '</tr>';

    //rewind($servers);
    foreach ( $servers as $server )
    {
      $server->ports = $db->get_results("SELECT portid, current_status FROM " . DB_TABLE_SERVER_PORT . " WHERE serverid='$server->serverid'");

      switch ($server->current_status) {
        case 0:
          $server_status_image = IMAGE_NEUTRAL;
          break;
        case 1:
          $server_status_image = IMAGE_ONLINE;
          break;
        case 2:
          $server_status_image = IMAGE_OFFLINE;
          break;
        case 3:
          $server_status_image = IMAGE_CAUTION;
          break;
        case 4:
          $server_status_image = IMAGE_MAINT;
          break;
        default:
          $server_status_image = IMAGE_NEUTRAL;
      }

      $server->groupid = $db->get_var("SELECT groupid FROM " . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server->serverid'");

      if ($server->groupid != $global->last_server_status_list_group_id) {
        if ($global->last_server_status_list_color_flag == 0) {
          $global->last_server_status_list_color_flag = 1;
          $global->last_server_status_list_color = '#FFFFFF';
        }
        else {
          $global->last_server_status_list_color_flag = 0;
          $global->last_server_status_list_color = '#E6E6E6';
        }

        $global->last_server_status_list_group_id = $server->groupid;
      }
      else {
        $global->last_server_status_list_group_id = $server->groupid;
      }

      $server_table_color = $global->last_server_status_list_color;

      echo '<tr bgcolor="' . $server_table_color . '" colspan="' . $grid_num_cols . '">
        <td align="center">' . $server->serverid . '</td>
        <td align="center"><img src="' . $images_url . '/' . $server_status_image . '"></td>
        <td align="center"><a href="server_history.php?serverid=' . $server->serverid . '" target="_blank">Up</a></td>
        <td align="center"><a href="server-edit.php?serverid=' . "$server->serverid\">$server->hostname" . '</a></td>
        ';
      //echo '<td align="center">';
      //print_r($server->ports);
      //echo '</td>' . "\n";

      foreach ( $intersecting_ports as $grid_port_id )
      {
        if ($server->ports) // this means: 'if the port is even assigned to the current server we are checking' ...
        {
          foreach ( $server->ports as $port )
          {
            if ($grid_port_id == $port->portid)
            {

              switch ($port->current_status) {
                case 0:
                  $grid_port_status_image = IMAGE_NEUTRAL;
                  break;
                case 1:
                  $grid_port_status_image = IMAGE_ONLINE;
                  break;
                case 2:
                  $grid_port_status_image = IMAGE_OFFLINE;
                  break;
                case 3:
                  $grid_port_status_image = IMAGE_CAUTION;
                  break;
                case 4:
                  $grid_port_status_image = IMAGE_MAINT;
                  break;
                default:
                  $grid_port_status_image = IMAGE_NEUTRAL;
              }

              echo '<td align="center"><img src="' . $images_url . '/' . $grid_port_status_image . '"></td>' . "\n";
              $match = 1;
            }
          }
        }

        if (!$match)
        {
          echo '<td align="center"><b>-</b></td>' . "\n";
        }
        $match = 0;
      }
    }

    echo '</tr></table>';
  }  // end $intersecting ports foreach

}


####################################
## Function check_server
##
function check_server($server_id, $port_id = 80) {

  global $db, $global_config;

  $chkstarttime = explode(' ', microtime());
  $chkstarttime = $chkstarttime[1] + $chkstarttime[0];

  $timeout = $global_config->chk_timeout;  // Seconds
  $timeout_secondary = $global_config->chk_timeout_secondary;  // Seconds
  //$timeout = 5;  // Seconds
  $leading_space = '&nbsp;&nbsp;&nbsp;&nbsp;';

	// this list should actually be comming from 'DB_TABLE_SERVER_PORT' rather than this function - be sure to update that. It will be less 'loop' (resource) intensive, less DB calls.
  $set_server_port_list = get_full_port_list($server_id);

  $current_server = $db->get_row("SELECT * FROM " . DB_TABLE_SERVERS . " WHERE serverid='$server_id'");
  $last_status = $current_server->current_status;
  echo $current_server->serverid . ' <b>' . $current_server->hostname . ' (' . $current_server->ip_address . ")</b><br>\n";
  $timestamp = time();

  $online_port_count = 0;
  //$add_comma = '';
  $num_of_ports = count($set_server_port_list);
  //print_r($set_server_port_list);
  $ports_down = '';
  $comma = '';

  if ($current_server->maint_mode == 0) {  // check for maintenance mode; 0 = means server is active
		// server is NOT in maintenance mode, proceed with script

		//print_r($current_server->maint_mode);
    // This will double check the IP address to see if it has been updated.

    //$current_ip_address = gethostbyname($current_server->hostname);
    $recorded_ip_address = $current_server->ip_address;
    //$current_server->ip_address = $current_ip_address;

    /*
    if ( $current_ip_address != $recorded_ip_address )
    {
      $sql = 'UPDATE ' . DB_TABLE_SERVERS . " SET ip_address='$current_ip_address' WHERE serverid='$server_id'";
      $success = $db->query($sql);
      if ($success == true) {
        echo $leading_space . 'Server data updated with a new IP address.<br>';
        // Add code here to add a log to other events.
      }
    }
    */

		// port check loop, will be cycled through for each server
    for ($j = 0; $j < $num_of_ports; $j++) {

      $timestamp = time();

      $port = $set_server_port_list[$j]['port'];
      $portid = $set_server_port_list[$j]['id'];

      $check = NULL;
      $errno = NULL;
      $errstr = NULL;

      $check[1] = @fsockopen ($recorded_ip_address, $port, $errno, $errstr, $timeout);

      if (!$check[1]) {
        echo $leading_space . 'Check 1 timed out, running secondary check<br>';
        $errno = NULL;
        $errstr = NULL;
        $check[2] = @fsockopen ($recorded_ip_address, $port, $errno, $errstr, $timeout_secondary);
      }

			$dataread = '';  // not sure what this variable was originally intended for but it's throwing errors if not set, so we're setting it :)
      if (!$check[1] && !$check[2])
      {
        if ($errno = 111){
          echo $leading_space . 'Connection to ' . $current_server->hostname . ' port ' . $port . " <font color=\"#CC0000\"><b>NOT successful</b></font> :: ERROR ($errno) -> $errstr<br>\n";
          $status = STATUS_OFFLINE;
          log_entry_port($server_id, $portid, $timestamp, $status, $errno, $errstr, $dataread);
        }
        else {
          echo $leading_space . 'Connection to ' . $current_server->hostname . ' port ' . $port . ' <font color="#CC0000"><b>NOT successful</b></font> after a timeout of ' . $timeout . " seconds :: ERROR ($errno) -> $errstr<br>\n";
          $status = STATUS_OFFLINE;
          log_entry_port($server_id, $portid, $timestamp, $status, $errno, $errstr, $dataread);
        }
        $ports_down .= $comma . $port;
        $comma = ', ';
      }
      else {
        echo $leading_space . 'Connection to ' . $current_server->hostname . ' port ' . $port . " <font color=\"#009900\"><b>successful</b></font>.<br>\n";
        if (isset($check[1]))
          fclose ($check[1]);
        if (isset($check[2]))
          fclose ($check[2]);
        $status = STATUS_ONLINE;
        $online_port_count++;
        log_entry_port($server_id, $portid, $timestamp, $status, $errno, $errstr, $dataread);
      }

    	$mtime = explode(' ', microtime());
    	$totaltime = $mtime[0] + $mtime[1] - $chkstarttime;
      $debug_vars = sprintf('[ Time: %.3fs | ' . $db->num_queries . ' Queries ]<br>', $totaltime);
      //echo $leading_space . 'Server Check Debug: ' . $debug_vars;

    }  // end for loop

    $a_status = log_entry_server($server_id, $timestamp, $num_of_ports, $online_port_count);
		//$notifyMsg = 'Server status has changed.';

  } // end check for maintenance mode
  else {  // if maintenance mode is on
    $a_status = log_entry_server($server_id, $timestamp, $num_of_ports, 0);
		//$notifyMsg = 'Server Maintenance Mode is enabled.';
    //notify_user($user_id, $server_id, $a_status, NULL, $message = 'Server Maintenance Mode is enabled.');
  }

  /* this section figures out what users to notify if a servers status changes */
  $server_groups = $db->get_results('SELECT * FROM ' . DB_TABLE_SERVER_GROUP . " WHERE serverid='$server_id'");
  if ($server_groups) {
    foreach ( $server_groups as $server_group ) {
      $users_in_group = $db->get_results('SELECT * FROM ' . DB_TABLE_USER_GROUP . " WHERE groupid='$server_group->groupid'");
      if ($users_in_group) {
        foreach ( $users_in_group as $user_in_group ) {
          $curGroupUser = $db->get_row('SELECT * FROM ' . DB_TABLE_USERS . " WHERE userid='$user_in_group->userid'");
          echo $leading_space . 'User To Notify: ';
          print_r($curGroupUser->username);
          echo '<br>';
          //print_r($a_status);
          //echo '<br>';
          notify_user($curGroupUser->userid, $server_id, $a_status, $ports_down);
        }
      }
    }
  }

	$mtime = explode(' ', microtime());
	$totaltime = $mtime[0] + $mtime[1] - $chkstarttime;
  $debug_vars = sprintf('[ Time: %.3fs | ' . $db->num_queries . ' Queries ]<br>', $totaltime);
  echo $leading_space . 'Server Check Debug: ' . $debug_vars;

}


####################################
## Function notify_user
##
function notify_user($user_id, $server_id, $status = '', $ports_down = '', $message = '') {

  global $db, $mailFromHeader;

  $sql = 'SELECT * FROM ' . DB_TABLE_SERVERS . " WHERE serverid='$server_id'";
  $serverinfo = $db->get_row($sql);

  if ($status['last'] != $status['current'])
  {

    $sql = 'SELECT * FROM ' . DB_TABLE_USERS . " WHERE userid='$user_id'";
    $msguserinfo = $db->get_row($sql);

    switch ($status['last']) {
      case 0:
        $last_status = 'Neutral/Default';
        break;
      case 1:
        $last_status = 'Online';
        break;
      case 2:
        $last_status = 'Offline';
        break;
      case 3:
        $last_status = 'Caution';
        break;
      case 4:
        $last_status = 'Maintenance Mode';
        break;
      default:
        $last_status = 'Neutral/Default';
    }

    switch ($status['current']) {
      case 0:
        $current_status = 'Neutral/Default';
        break;
      case 1:
        $current_status = 'Online';
        break;
      case 2:
        $current_status = 'Offline';
        break;
      case 3:
        $current_status = 'Caution';
        break;
      case 4:
        $current_status = 'Maintenance Mode';
        break;
      default:
        $current_status = 'Neutral/Default';
    }

    if ($current_status == 'Online')
    {
      $ports_email_message = "All ports reported up.\n";
      $ports_email_message = "All ports up.\n";
    }
    else
    {
      $ports_email_message = "The following service ports are reported as down:\n";
      $ports_email_message = "Ports down: \n";
			if ($status['current'] == 4 && !$ports_down);  $ports_down = '  Not being checked due to maintenance.';
    }


    $mail_report = 'Server > ' . $serverinfo->hostname . ' < has just changed status from ' . $last_status . ' to ' . $current_status . " status.\n\n" .
      $message . $ports_email_message . $ports_down;

		$ports_sms_message = '';
    $sms_report = 'netMon: ' . $serverinfo->hostname . "\nStatus: " . $current_status . "\n\n" . $ports_sms_message . $ports_down;

    if ($msguserinfo->email_notify == 1) {
      mail($msguserinfo->email, 'Server Alert for ' . $serverinfo->hostname, $mail_report, $mailFromHeader);
      echo 'Sending message: ' . $mail_report . '<br>';
    }
    if ($msguserinfo->sms_notify == 1) {
      if (strstr($msguserinfo->sms, '@'))
      {
        mail($msguserinfo->sms, '', $sms_report, "From: NetMon <$mailFrom>");
        echo 'Sending SMS message: ' . $sms_report . '<br>';
      }
      else {
        // this is where we would send an SMS to a phone number directly through an SMS server.
      }
    }

  }

  //echo 'Are we mailing user? ' . $last_status . ' - ' . $current_status . '<br>';
  //echo 'notify user for server: ' . $serverinfo->hostname . '<br>';

}



// ##################################
// # Function log_entry_server    #
// ##################################
function log_entry_server($server_id, $timestamp, $num_of_ports, $ports_online) {

  global $db;

  $leading_space = '&nbsp;&nbsp;&nbsp;&nbsp;';

  $lastserverinfo = $db->get_row("SELECT statusid, status FROM " . DB_TABLE_SERVER_LOG . " WHERE serverid='$server_id' ORDER BY time DESC");
	if (!isset($lastserverinfo) || empty($lastserverinfo)) {
		$lastserverinfo = new StdClass;
		$lastserverinfo->status = "no historic data found, first time you've checked this server?";
	}

  $last_status = $lastserverinfo->status;
  $maint_mode = $db->get_var("SELECT maint_mode FROM " . DB_TABLE_SERVERS . " WHERE serverid='$server_id'");
  $historic_data = $db->get_row("SELECT * FROM " . DB_TABLE_HISTORIC_LOG . " WHERE serverid='$server_id' ORDER BY `year_month` DESC");
	if (!isset($historic_data) || empty($historic_data)) {
		$historic_data  = new StdClass;
		$historic_data->year_month = "no historic data found, first time you've checked this server?";
		$historic_data->online = "no historic data found, first time you've checked this server?";
		$historic_data->offline = "no historic data found, first time you've checked this server?";
		$historic_data->caution = "no historic data found, first time you've checked this server?";
		$historic_data->maint = "no historic data found, first time you've checked this server?";
		$historic_data->year_month = "no historic data found, first time you've checked this server?";
	}
  $current_yearmonth = getdate($timestamp);
  if ($current_yearmonth['mon'] <= 9)
    $current_month = '0' . $current_yearmonth['mon'];
  else
    $current_month = $current_yearmonth['mon'];
  $current_year = $current_yearmonth['year'];
  $current_yearmonth = $current_year . $current_month;
  $historic_yearmonth = $historic_data->year_month;

  //print_r($historic_data);

  $historic_online = $historic_data->online ? $historic_data->online : 0;
  $historic_offline = $historic_data->offline ? $historic_data->offline : 0;
  $historic_caution = $historic_data->caution ? $historic_data->caution : 0;
  $historic_maint = $historic_data->maint ? $historic_data->maint : 0;

  if ($maint_mode == 1) {
    $current_status = STATUS_MAINT;
    $historic_maint++;
  }
  elseif ($ports_online == $num_of_ports) {
    $current_status = STATUS_ONLINE;
    $historic_online++;
  }
  elseif ($ports_online < $num_of_ports && $ports_online > 0) {
    $current_status = STATUS_CAUTION;
    $historic_caution++;
  }
  elseif ($ports_online == 0) {
    $current_status = STATUS_OFFLINE;
    $historic_offline++;
  }

	if ($num_of_ports == 0)
		$num_of_ports = '<span style="color: #CC0000">ZERO ports are assigned to this server, nothing to check.</span>';
	echo $leading_space . 'num of ports assigned: ' . $num_of_ports . '<br>';
  echo $leading_space . 'ports online: ' . $ports_online . '<br>';
  echo $leading_space . 'status: ' . $current_status . '<br>';
  echo $leading_space . 'last status: ' . $last_status . '<br>';
  echo $leading_space . 'year month: ' . $current_yearmonth . '<br>';
  echo $leading_space . 'historic year month: ' . $historic_yearmonth . '<br>';

  // Notice that unlike in log_entry_port, there is no ! in front of strcmp. Which means if last_status and current_status are different, do the following.
  if(strcmp($last_status, $current_status)) {  // Check - is last status the same as current status? If not then execute following ...
    $sql = 'INSERT INTO ' . DB_TABLE_SERVER_LOG . ' (serverid, time, status) VALUES ' . "('$server_id', '$timestamp', '$current_status')";
    $success = $db->query($sql);
    $sql = 'UPDATE ' . DB_TABLE_SERVERS . " SET current_status='$current_status' WHERE serverid='$server_id'";
    $success = $db->query($sql);
  }

  // Update historic checks.
  if ($current_yearmonth != $historic_yearmonth) {
    // We need to run this again because we are starting a new month, all values reset to 0 for this month.
    $historic_online = 0;
    $historic_offline = 0;
    $historic_caution = 0;
    $historic_maint = 0;
    if ($maint_mode == 1)
      $historic_maint = 1;
    elseif ($ports_online == $num_of_ports)
      $historic_online = 1;
    elseif ($ports_online < $num_of_ports && $ports_online > 0)
      $historic_caution = 1;
    elseif ($ports_online == 0)
      $historic_offline = 1;

    echo $leading_space . 'yearmonth ARE NOT equal<br>';
    $sql = 'INSERT INTO ' . DB_TABLE_HISTORIC_LOG . " (serverid, `year_month`, online, offline, caution, maint) VALUES ('$server_id', '$current_yearmonth', '$historic_online', '$historic_offline', '$historic_caution', '$historic_maint')";
    $success = $db->query($sql);
  }
  else {
    echo $leading_space .'yearmonth ARE equal<br>';
    $sql = 'UPDATE ' . DB_TABLE_HISTORIC_LOG . "
      SET online='$historic_online', offline='$historic_offline', caution='$historic_caution', maint='$historic_maint'
      WHERE statusid='$historic_data->statusid'";
    $success = $db->query($sql);
  }

  $a_status = array("last" => $last_status, "current" => $current_status);
  return $a_status;
}


#####################################
## Function log_entry_port
## Makes a log entry into database as to port status
##
function log_entry_port($server_id, $portid, $timestamp, $status, $errno, $errstr, $dataread) {

  global $db;

  $lastportinfo = $db->get_row("SELECT statusid, status, numchecks FROM " . DB_TABLE_STATUS_LOG . " WHERE serverid='$server_id' AND portid='$portid' ORDER BY time DESC");
	if (isset($lastportinfo) && !empty($lastportinfo)) {
		$statusid = $lastportinfo->statusid;
		$laststatus = $lastportinfo->status;
		$numchecks = $lastportinfo->numchecks;
	}
	else {
		$statusid = 0;
		$laststatus = 0;
		$numchecks = 0;
	}
  $numchecks++;

  //print_r($lastportinfo);

  if(!strcmp($laststatus, $status)) {  // Check - is last status the same as current status.
    $sql = 'UPDATE ' . DB_TABLE_STATUS_LOG . " SET numchecks='$numchecks' WHERE statusid='$statusid'";
    $success = $db->query($sql);
    $sql = 'UPDATE ' . DB_TABLE_SERVER_PORT . " SET current_status='$status' WHERE serverid='$server_id' AND portid='$portid'";
    $success = $db->query($sql);
  }
  else {  // If status is not the same as last insert a new log entry.
    $sql = 'INSERT INTO ' . DB_TABLE_STATUS_LOG . ' (serverid, portid, time, status, errcode, error, dataread, numchecks)
      VALUES ' . "('$server_id', '$portid', '$timestamp', '$status', '$errno', '$errstr', '', $numchecks)";
    $success = $db->query($sql);
    $sql = 'UPDATE ' . DB_TABLE_SERVER_PORT . " SET current_status='$status' WHERE serverid='$server_id' AND portid='$portid'";
    $success = $db->query($sql);
    //echo $sql;
  }

}


###############################################
## Function get_full_port_list
## Return an array of ports to be checked
##
function get_full_port_list($server_id, $onlyid = FALSE) {

	//echo 'GET FULL PORT LIST HAS BEEN CALLED: BEGIN<br>';

  global $db, $userinfo, $multiple_ports;  // this 'multiple_ports' is used to tell the 'edit server' page how many redundant/overlapping ports there were

  $serverid = $server_id;

		/* get list of ports that are in groups */
		$add_comma = '';
		$server_port_list = '';
		$i = 0;
		$multiple_ports = 0;

		$server_pgroups = $db->get_results("SELECT * FROM " . DB_TABLE_SERVER_PGROUP . " WHERE serverid='" . $serverid . "'");
		if ($server_pgroups) {
			foreach ( $server_pgroups as $server_pgroup )
			{
				$set_server_port_groups = $db->get_results("SELECT * FROM " . DB_TABLE_PORT_GROUP . " WHERE pgroupid='" . $server_pgroup->pgroupid . "'");

				$curr_pgroupname = $db->get_var("SELECT pgroupname FROM " . DB_TABLE_PGROUPS . " WHERE pgroupid='" . $server_pgroup->pgroupid . "'");
				if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) { // ###### BEGIN ADMIN DEBUG ######
					echo '<b>New Group ' . $curr_pgroupname . ': </b>';
					print_r ($set_server_port_groups);
					echo ' <br><br> ';
				}                                                 // ###### END ADMIN DEBUG ######

				if (isset($set_server_port_groups) && !empty($set_server_port_groups)) {
					foreach ( $set_server_port_groups as $set_server_port_group )
					{
						$assign_port = TRUE;
						$curr_server_port = $db->get_row("SELECT * FROM " . DB_TABLE_PORTS . " WHERE portid='" . $set_server_port_group->portid . "'");

						if (isset($set_server_port_list) && !empty($set_server_port_list))
							$arrayLength = count($set_server_port_list);
						else
							$arrayLength = 0;
						for ($k = 0; $k < $arrayLength; $k++){
							if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) { // ###### BEGIN ADMIN DEBUG ######
								print '<br>comparison - ' . $curr_server_port->portid . ' :: ' . $set_server_port_list[$k]['id'];
							}                                                 // ###### END ADMIN DEBUG ######
							if (isset($set_server_port_list[$k]['id']) && $curr_server_port->portid == $set_server_port_list[$k]['id']) {
								$assign_port = FALSE;
								if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) { // ###### BEGIN ADMIN DEBUG ######
									print '&larr;';
								}                                                 // ###### END ADMIN DEBUG ######
								$multiple_ports++;
							}
						}

						if ($assign_port == TRUE && $onlyid == FALSE) {
								$set_server_port_list[$i] = array("id"=>$curr_server_port->portid, "name"=>$curr_server_port->portname, "port"=>$curr_server_port->port);
								$i++;
						}
						elseif ($assign_port == TRUE && $onlyid == TRUE) {
								$set_server_port_list[$i] = $curr_server_port->portid;
								$i++;
						}

						// ###### BEGIN ADMIN DEBUG ######
						if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) {
							//$set_server_port_list .= $add_comma . $curr_server_port->portname . ' (' . $curr_server_port->port . ')';
							$add_comma = ', <br>';
							echo '<br>';
							print_r ($curr_server_port);
							echo '<br>' . $assign_port . '<br>';
							echo ' <br><br> ';
						}	 // ###### END ADMIN DEBUG ######
					}
				}

				// ###### BEGIN ADMIN DEBUG ######
				if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) {
					echo ' <hr> ';
				}	 // ###### END ADMIN DEBUG ######
			}
		}


		/* get a list of individual ports */
		$k = 0;
		//$multiple_ports = 0;
		$set_server_indv_ports = $db->get_results("SELECT * FROM " . DB_TABLE_SERVER_INDV_PORT . " WHERE serverid='" . $serverid . "'");

		//$curr_indvportname = $db->get_var("SELECT portname FROM " . DB_TABLE_PGROUPS . " WHERE pgroupid='" . $server_pgroup->pgroupid . "'");

		//print_r($set_server_indv_ports);
		//echo ' <br><br> ';

		if ($set_server_indv_ports) {
			foreach ( $set_server_indv_ports as $set_server_indv_port )
			{
				$assign_port = TRUE;
				//$curr_server_indv_port = $db->get_row("SELECT * FROM " . DB_TABLE_SERVER_INDV_PORT . " WHERE portid='" . $set_server_indv_port->portid . "'");

				$curr_server_port = $db->get_row("SELECT * FROM " . DB_TABLE_PORTS . " WHERE portid='" . $set_server_indv_port->portid . "'");

					// ###### BEGIN ADMIN DEBUG ######
				if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) {
					echo '<b>New Port ' . $curr_server_port->portname . ': </b>';
					//print_r ($set_server_port_groups);
					//echo ' <br><br> ';
				}	 // ###### END ADMIN DEBUG ######

				$arrayLength = count($set_server_port_list);
				for ($k = 0; $k < $arrayLength; $k++){
					// ###### BEGIN ADMIN DEBUG ######
					if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) {
						print '<br>indv_comparison - ' . $curr_server_port->portid . ' :: ' . $set_server_port_list[$k][id];
					}	 // ###### END ADMIN DEBUG ######
					if ($curr_server_port->portid == $set_server_port_list[$k][id]){
						$assign_port = FALSE;
						// ###### BEGIN ADMIN DEBUG ######
						if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) {
							print '&larr;';
						}	 // ###### END ADMIN DEBUG ######
						$multiple_ports++;
					}
				}

				if ($assign_port == TRUE && $onlyid == FALSE) {
						$set_server_port_list[$i] = array("id"=>$curr_server_port->portid, "name"=>$curr_server_port->portname, "port"=>$curr_server_port->port);
						$i++;
				}
				elseif ($assign_port == TRUE && $onlyid == TRUE) {
						$set_server_port_list[$i] = $curr_server_port->portid;
						$i++;
				}

				//$set_server_port_list .= $add_comma . $curr_server_port->portname . ' (' . $curr_server_port->port . ')';
				//$add_comma = ', <br>';
				if (ADMIN_DEBUG && $userinfo->admin_lvl >= 500) { // ###### BEGIN ADMIN DEBUG ######
					echo '<br>';
					print_r ($curr_server_port);
					echo '<br>' . $assign_port . '<br>';
					echo ' <br><br> ';
				}                                                 // ###### END ADMIN DEBUG ######
			}
		}
	$set_server_port_list = isset($set_server_port_list) ? $set_server_port_list : null;
	//echo 'GET FULL PORT LIST HAS BEEN CALLED: END<br>';
	//print_r($set_server_port_list);
	return $set_server_port_list;
}

?>
